/*
 * CONTROLE DE ESCOPO
 * Projeto: Silimed
 * Desenvolvimento: GM5
 */

var attrController = $('main[data-controller]').attr('data-controller');

var Controller = {
    getController: function () {
        if ($('main[data-controller]').length > 0) {
            eval('Controller.' + attrController + '();');
        }
    },
    global: function(){
        Util.menuMobile();
        Util.scrollMenu();
        Util.sidebardropdonw();
        Util.tooltipDefault(); 
        Util.tooltipBoot();
        Util.scrollBtnTop();
        
    },
    home: function(){
        Util.corousel();
    // Util.bannerMargin();
    },
    quemSomos: function(){
        
    },
    produtos: function(){
        
    },
    pedidoDeCompra: function(){
        
    },
    estudoClinico: function(){
        
    },

    pacientes: function(){
        
    },
    carreiras: function(){
        
    },
    contato: function(){
        Util.tooltipBoot(); 
        Util.formContato();      
    },
    menuCopy: function(){
        Util.sidebardropdonw();
    },
    // subpages
    certificacoes: function() {

    },
    nossaHistoria: function() {

    },
    implantesMamarios: function() {

    },
    implantesDeContornoCorporal: function() {

    },

    implantesFaciaisDeMento:function() {

    },
    implantesDaLinhaDeUrologia: function() {

    },
    modeladorNasal: function(){

    },
    expansoresDeTecido: function(){

    },
    balaoGastrico: function(){

    },
    paraMedicos: function(){

    },
    paraPacientes: function(){

    },
    medgel: function(){

    },
    ondeEstamos: function(){
        Util.mapsMarker();
    },
    segurancaParaVoce: function(){
        Util.formRegister(); 
        Util.buscaCEP();
    },
};

jQuery(document).ready(function ($) {
    Util.aos();
    Controller.global();
    Controller.getController();
});

(console.info || console.log).call(console, "%c<Dev by 🐱 GM5 Team/>", "color: green; font-weight: bold;");
