/*
 * FUNÇÕES NECESSÁRIAS
 * Projeto: Silimed
 * Desenvolvimento: GM5
 */

var Util = {
    menuMobile: function(){  

        var open = $('.btn-open span');
        var close = $('.btn-close button');
        var menu = $('.banner-menu .menu-nav');
        var dropdownResponsive = $(".animated__child span");
        
        $(open).on('click', function(e){
            e.preventDefault();
            $(menu).toggle("slide");
            $(open).hide();
            $('html, body').css({
                overflow: 'hidden',
                height: '100%'
            });
            $('.mask-opacity').show();
        });
        $(close).on('click', function(e){
            e.preventDefault();
            $(menu).toggle("slide");
            $(open).show();
            $(dropdownResponsive).removeClass('clicled');
            $('html, body').css({
                overflow: 'auto',
                height: 'auto'
            });
            $('.mask-opacity').hide();
        });

        // $(dropdownResponsive).click(function() {
        //     var dataTarget = $(this).data("target");
        //     debugger;
        //     $(dataTarget).slideUp(200);
        //     if (
        //         $(this)
        //         .parent()
        //         .hasClass("clicled")
        //     ) {
        //         $(dropdownResponsive).removeClass("clicled");
        //         $(this)
        //         .parent()
        //         .removeClass("clicled");
        //     } else {
        //         $(dropdownResponsive).removeClass("clicled");
        //         $(this)
        //         .next(dataTarget)
        //         .slideDown(200);
        //         $(this)
        //         .parent()
        //         .addClass("clicled");
        //     }
        // });

        $(dropdownResponsive).on('click',function(e){
            e.preventDefault();
            console.log('clicou');
            // $(dropdownResponsive).removeClass('clicled');
            var dataTarget = $(this).data("target");
            $(this).toggleClass('clicled');
            
            if(!$(this).hasClass('clicled')){            
                $(dataTarget).css('display', 'none');
                $(dropdownResponsive).removeClass('clicled');
            }   

            if($(this).hasClass('clicled')){
                $(dataTarget).css('display', 'flex');
                
            }                  
        });

    },
    sidebardropdonw: function(){
        var menucopy = $(".sidebar-dropdown__haschild span");
        var maskopacity = $('.mask-opacity');
        var totalHeightPage = $(document).height();

        $(maskopacity).css("height", totalHeightPage + 100);


        $(menucopy).on('click',function(e) {
            e.preventDefault();
            console.log('clicou 1');
            $(".sidebar-submenu").slideUp(200);
            if (
                $(this)
                .parent()
                .hasClass("active")
            ) {
                $(".sidebar-dropdown__haschild").removeClass("active");
                $(this)
                .parent()
                .removeClass("active");
            } else {
                $(".sidebar-dropdown__haschild").removeClass("active");
                $(this)
                .next(".sidebar-submenu")
                .slideDown(200);
                $(this)
                .parent()
                .addClass("active");
            }
        });

        $("#close-sidebar").click(function(e) {
            e.preventDefault();
            $(".page-wrapper").removeClass("toggled");
            $('.mask-opacity').hide();
        });
        $("#show-sidebar").click(function(e) {
            e.preventDefault();
            $(".page-wrapper").addClass("toggled");
            $('.mask-opacity').show();
        });
    },
    scrollBtnTop: function() {
        var btn = $('#button');

        $(window).scroll(function() {
            if ($(window).scrollTop() > 300) {
                btn.addClass('show');
            } else {
                btn.removeClass('show');
            }
        });

        btn.on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({scrollTop:0}, '300');
        });


    },
    scrollMenu: function(){        
        // var menuHeight = $('#silimed-header').height();
        // var imgMenu = $('.logo-silimed img');          
        // $(window).bind('scroll', function () {
        //     if ($(window).scrollTop() > menuHeight) {
        //         $('.menu-nav').addClass('menu-nav__fixed');
        //         $(imgMenu,'picture source').hide();
        //     } else {
        //         $('.menu-nav').removeClass('menu-nav__fixed');
        //         $(imgMenu,'picture source').show();
        //     }
        // });

        var menuHeight = $('#silimed-header').height();
        // var imgMenu = $('.sillimed-menu-logo img');          
        $(window).bind('scroll', function () {
            if ($(window).scrollTop() > menuHeight) {
                $('.sidebar-wrapper').addClass('sidebar-wrapper__fixed');
                $('.menu-responsive-silimed').addClass('menu-responsive-silimed__fixed');
                $(imgMenu,'picture source').hide();
            } else {
                $('.sidebar-wrapper').removeClass('sidebar-wrapper__fixed');
                $('.menu-responsive-silimed').removeClass('menu-responsive-silimed__fixed');
                // $(imgMenu,'picture source').show();
            }
        });
    },
    corousel: function(){
        
        var swiper = new Swiper(".mySwiper", {
            loop: true,        
            pagination: {
                el: ".swiper-pagination",
                type: "fraction",            
                renderFraction: function (currentClass, totalClass) {
                    return '<span class="sizeCurrent">0</span> <span class="' + currentClass + '"></span>' +
                            "<div class='divisorItem'><hr></div> " +
                            '<span>0</span> <span class="' + totalClass + '">0</span>';
                }        
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            fadeEffect: { crossFade: true },
            virtualTranslate: true,
            autoplay: {
                delay: 1500,
                disableOnInteraction: true,
            },
            speed: 1000, 
            slidersPerView: 1,
            effect: "fade"
        });
        
        swiper.on('slideChange', function () {
            if(this.activeIndex) {
                $(".toDown").addClass('toTop');
            }
        });
        
        var toTop = $(".swiper-slide-active");
        $(toTop).removeClass('toTop');

        // this.bannerMargin();

        var homeSwiper = new Swiper(".homeSwiper", {
            // slidesPerView: 3,
            speed: 1500,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            breakpoints: {
                768: {
                    slidesPerView: 1,
                },
                769: {
                    slidesPerView: 3,
                },
            },
        });


        var instaSwiper = new Swiper(".instaSwiper", {
            loop: true,   
            speed: 200,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },

            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },

            breakpoints: {
                769: {
                    slidesPerView: 5,
                },
                768: {
                    slidesPerView: 3,
                },
            },

        });

    },
    bannerMargin: function(){
         // carousel margin
        var viewPort =   $(window).height();
        var bannerImg = $(".banner-carrousel img");
        var produtoCarousel = $(".produto-carousel");    
        $(bannerImg).css("height", viewPort);
        $(produtoCarousel).css("margin-top", (viewPort/2)+ 30);
    },

    tooltipBoot: function(){
        $('[data-toggle="tooltip"]').tooltip();
    },
    tooltipDefault: function(){
        $('[data-toggle="tooltip-default"]').tooltip();
    },

    formContato: function(){
        var form = $('form#formContato');        
        form.submit(function( event ) {
            event.preventDefault();
            
            $.ajax({
                    type: "POST",                
                    url: '/contato',                
                    data: form.serialize(),
                    success: function(data){  
                        
                    }
            });    
        });
        // form.submit(function(e) {
        //     e.preventDefault();
        //     $.ajax({
        //         type: "POST",                
        //         url: '/contato',                
        //         data: form.serialize(),
        //         success: function(data){
                    
        //         }
        //     });                             
        // })
    },

    mapsMarker: function(){
        // Initialize and add the map
    },

    formRegister: function(){
        
        var form = $('form#formRegistro'); 
        $.getJSON('./assets/js/paises-gentilicos-google-maps.json', function(json){
            var arrayCountry = [];
            for (var key in json) {
                if (json.hasOwnProperty(key)) {
                    var item = json[key];
                    arrayCountry.push({
                            name_int: item.sigla,
                            name_br: item.nome_pais
                        });            
                    }
            }
            var selectList = $('#inputRoleCountry');
            var selectSurgeryList = $('#inputRoleSurgeryCountry');
            for (var i = 0; i <= arrayCountry.length; i++) {
                selectList.append('<option value="' + arrayCountry[i].name_int + '">' + arrayCountry[i].name_br + '</option>');
                selectSurgeryList.append('<option value="' + arrayCountry[i].name_int + '">' + arrayCountry[i].name_br + '</option>');

            }
        });       

        form.submit(function( event ) {
            event.preventDefault();
            $.ajax({
                    type: "POST",                
                    url: '/contato',                
                    data: form.serialize(),
                    success: function(data){  
                        
                    }
            });    
        });
        // form.submit(function(e) {
        //     e.preventDefault();
        //     $.ajax({
        //         type: "POST",                
        //         url: '/contato',                
        //         data: form.serialize(),
        //         success: function(data){
        //         }
        //     });                             
        // })
    },

    buscaCEP: function(){
        $(document).ready(function() {

            function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#rua").val("");
                $("#bairro").val("");
                $("#cidade").val("");
                $("#uf").val("");
                $("#ibge").val("");
            }
            
            //Quando o campo cep perde o foco.
            $("#cep").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#rua").val("...");
                        $("#bairro").val("...");
                        $("#cidade").val("...");
                        $("#uf").val("...");
                        $("#ibge").val("...");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#rua").val(dados.logradouro);
                                $("#bairro").val(dados.bairro);
                                $("#cidade").val(dados.localidade);
                                $("#uf").val(dados.uf);
                                $("#ibge").val(dados.ibge);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });

        
    },
    aos: function (){
        AOS.init({
            easing: 'ease-in-out-sine',
            duration: 900,
        });
    },
    

}