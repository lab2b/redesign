<?php
function getPage(){

  $url = $_SERVER['REQUEST_URI'];
  $metodo = $_SERVER["REQUEST_METHOD"];

  if($metodo =='GET'){

    switch ($url) {
      case '/':
        include('pages/home.php');
      break; 

      case '/carreiras':
        include('pages/carreiras.php');
      break;

      case '/contato':
        include('pages/contato.php');
      break;

      case '/estudo-clinico':
        include('pages/estudo-clinico.php');
      break;  

      case '/teste':
        include('pages/teste.php');
      break;  
              
      case '/pacientes':
        include('pages/pacientes.php');
      break; 

      case '/pedidos-de-compra':
        include('pages/pedidos-de-compra.php');
      break;    

      
      case '/produtos':
        include('pages/produtos.php');
      break; 

      case '/quem-somos':
        include('pages/quem-somos.php');
      break; 

      case '/estudo-clinico':
        include('pages/estudo-clinico.php');
      break; 

      case '/linha-standard':
        include('pages/linha-standard.php');
      break;

      case '/menu-copy':
        include('pages/menu-copy.php');
      break; 

      // subpages
      case '/balao-gastrico':
        include('pages/subpages/balao-gastrico.php');
      break; 
      case '/certificacoes':
        include('pages/subpages/certificacoes.php');
      break; 
      case '/expansores-de-tecido':
        include('pages/subpages/expansores-de-tecido.php');
      break; 
      case '/implantes-da-linha-de-urologia':
        include('pages/subpages/implantes-da-linha-de-urologia.php');
      break; 
      case '/implantes-de-contorno-corporal':
        include('pages/subpages/implantes-de-contorno-corporal.php');
      break; 
      case '/implantes-faciais-de-mento':
        include('pages/subpages/implantes-faciais-de-mento.php');
      break; 
      case '/implantes-mamarios':
        include('pages/subpages/implantes-mamarios.php');
      break; 
      case '/medgel':
        include('pages/subpages/medgel.php');
      break; 
      case '/modelador-nasal':
        include('pages/subpages/modelador-nasal.php');
      break; 
      case '/nossa-historia':
        include('pages/subpages/nossa-historia.php');
      break; 
      case '/onde-estamos':
        include('pages/subpages/onde-estamos.php');
      break; 
      case '/para-medicos':
        include('pages/subpages/para-medicos.php');
      break; 
      case '/para-pacientes':
        include('pages/subpages/para-pacientes.php');
      break; 
      case '/psps':
        include('pages/subpages/psps.php');
      break; 
      case '/seguranca-para-voce':
        include('pages/subpages/seguranca-para-voce.php');
      break; 
      
      
      
      default:
        include('pages/home.php');
        break;
        
        
    }
  }
}