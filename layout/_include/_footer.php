<footer id="silimed-footer">
    <!-- Back to top button -->
    <a id="button" class="hi-icon-effect-5c">
        <!-- <i class="fas fa-long-arrow-alt-up"></i> -->
        <i class="hi-icon fal fa-long-arrow-up"></i>
        
    </a>

    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <div class="logo-footer">
                    <img src="./assets/imagem/logos-silimed.png" alt="" srcset="">
                </div>
                <div class="social-icons-footer">
                    <ul>
                        <li>
                            <a href="http://">
                                <i class="fab fa-facebook-square"></i>
                            </a>
                        </li>
                        <li>
                            <a href="http://">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="http://">
                            <i class="fab fa-linkedin"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2 col-sm-12">
                <ul class="footer-list footer-list__first">
                    <li>
                        <a href="/quem-somos">
                            Quem somos
                        </a>
                    </li>
                </ul>
                <ul class="footer-list">
                    <li>
                        <a href="/quem-somos">
                            Nossa História
                        </a>
                    </li>
                    <li>
                        <a href="/quem-somos">
                            Certificações
                        </a>
                    </li>
                    <li>
                        <a href="/quem-somos">
                            Onde Estamos
                        </a>
                    </li>
                </ul>

            </div>
            <div class="col-md-3 col-sm-12">
                <ul class="footer-list footer-list__first">
                    <li>
                        <a href="/quem-somos">
                            Produtos
                        </a>
                    </li>
                </ul>
                <ul class="footer-list">
                    <li>
                        <a href="/quem-somos">
                            Implantes Mamários
                        </a>
                    </li>
                    <li>
                        <a href="/quem-somos">
                            Implantes de Contorno Corporal
                        </a>
                    </li>
                    <li>
                        <a href="/quem-somos">
                            Implantes de Linha de Urologia
                        </a>
                    </li>
                    <li>
                        <a href="/quem-somos">
                            Implantes Faciais (de mento)
                        </a>
                    </li>
                    <li>
                        <a href="/quem-somos">
                            Expansores de Tecido
                        </a>
                    </li>
                    <li>
                        <a href="/quem-somos">
                            Medgel(Tratamento de Cicatrizes)
                        </a>
                    </li>
                    <li>
                        <a href="/quem-somos">
                            Balão Gástrico
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-12">
                <ul class="footer-list footer-list__first">
                    <li>
                        <a href="/quem-somos">
                        Pedido de compra
                        </a>
                    </li>
                </ul>
                <ul class="footer-list">
                    <li>
                        <a href="/quem-somos">
                            Pacientes
                        </a>
                    </li>
                    <li>
                        <a href="/quem-somos">
                            Carreiras
                        </a>
                    </li>
                    <li>
                        <a href="/quem-somos">
                            Contato
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-12">
                <ul class="footer-list footer-list__first">
                    <li>
                        <a href="/quem-somos">
                            Termos
                        </a>
                    </li>
                </ul>
                <ul class="footer-list">
                    <li>
                        <a href="/quem-somos">
                            Termos de Uso
                        </a>
                    </li>
                    <li>
                        <a href="/quem-somos">
                            Política de Privacidade
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="line-info">
        <div class="container">
            <div class="tel-info">
                <ul>
                    <li>
                        <a href="tel:08009424199">SAC: 0800 942 4199 </a>
                    </li>
                    <li>
                        <a href="tel:08006031500">Comercial: 0800 603 1500</a>
                    </li>
                    <li>
                        <a href="/carreiras">Trabalhe Conosco</a>
                    </li>
                    <li>
                        <a href="/carreiras">Sales Lounge</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

<script src="./assets/libs/aos/aos.js"></script>
<script src="./assets/js/controller.js"></script>
<script src="./assets/js/classes.js"></script>



</body>
</html>

