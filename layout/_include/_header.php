<!doctype html>
<html>
    <head>	
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="https://gmpg.org/xfn/11">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
        <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css" />
        <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
        <link rel="stylesheet" href="./assets/libs/aos/aos.css">
        <link rel="stylesheet" href="./assets/css/main.css">

        <link rel="stylesheet" id="font-awesome-pro-css" href="./assets/fonts/fontawesome-all.min.css">
    </head>
<body>
    <!-- mascara do menu responsivo -->
    <div class="mask-opacity"></div>

    <header id="silimed-header">
        <div class="banner-menu">            
            <!-- componente menu topo -->
            <div class="container">
                <div class="row">

                    <div class="language-login">
                        <nav>
                            <!-- menu idiomas -->
                            <ul class="language-list">
                                <li class="language-item language-item--english">
                                    <a href="http://">
                                        <span> Inglês </span>   
                                    </a>
                                </li>
                                <li class="language-item language-item--spanish">
                                    <a href="http://">
                                        <span> Español </span>
                                    </a>
                                </li>
                                <li class="language-item language-item--italian">
                                    <a href="http://">
                                    <span> Italiano </span>
                                    </a>
                                </li>
                            </ul>
                        </nav>

                        <!-- Silimed academy -->
                        <div class="acamedemy-btn">
                            <a href="http://"> 
                                Silimed Acamedemy
                            </a>
                        </div>

                        <!-- redes sociais itens -->
                        <div class="social-icons">
                            <ul>
                                <li>
                                    <a href="http://">
                                        <span  class="red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Facebook">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>

                                <li>
                                    <a href="http://">                                        
                                        <span  data-toggle="tooltip-default" data-placement="bottom" title="Instagram">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </li>

                                <li>
                                    <a href="http://">
                                        <span  data-toggle="tooltip-default" data-placement="bottom" title="Linkedin">
                                        <i class="fab fa-linkedin"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <!-- login item -->
                        <div class="login-area">
                            <a href="http://">
                                <img src="./assets/imagem/icons/login.svg" alt="" srcset="">
                                <span> 
                                    Login
                                </span> 
                            </a>
                        </div>

                    </div>
                </div>
            </div>
            <hr>
            <!-- componente main menu -->
            <div class="page-wrapper chiller-theme ">
                <div class="menu-responsive-silimed">
                    <!-- Logo e icone hamburger menu -->
                    <a class="hide-fixed-responsive" href="/home">
                        <img srcset="./assets/imagem/logos-silimed.png" alt="Logo Silimed">
                    </a>
                    <a class="show-fixed-responsive" href="/home">
                        <img srcset="./assets/imagem/logos-silimed-colorido.png" alt="Logo Silimed">
                    </a>
                    <a href="#" id="show-sidebar" class="box-shadow-menu "></a>
                </div>

                <nav id="sidebar" class="sidebar-wrapper"> 
                    <div class="sidebar-content">
                        <!-- Cabeçalho menu responsivo -->
                        <div class="sidebar-brand">
                            <a href="/home">
                                <picture>
                                    <source srcset="./assets/imagem/logos-silimed-colorido.png" media="(max-width: 767px)">
                                    <source srcset="./assets/imagem/logos-silimed.png">
                                    <img srcset="./assets/imagem/logos-silimed.png" alt="Logo Silimed">
                                </picture>
                            </a>
                            <div id="close-sidebar">
                                <i class="fas fa-times"></i>
                            </div>
                        </div>
                        <!-- Itens do menu -->
                        <div class="sidebar-menu">
                            <ul>
                                <!-- logo -->
                                <li>
                                    <div class="silimed-menu-logo">
                                        <a href="/home">
                                            <img class="hide-fixed" srcset="./assets/imagem/logos-silimed.png" alt="Logo Silimed">
                                            <img class="show-fixed"srcset="./assets/imagem/logos-silimed-colorido.png" alt="Logo Silimed">
                                        </a>
                                    </div>
                                </li>
                                <!-- links -->
                                <li class="sidebar-dropdown sidebar-dropdown__haschild animated animated__child">
                                    <a href="/quem-somos" class="hover hover-1">
                                        Quem Somos
                                    </a>
                                    <span></span>
                                    <div class="sidebar-submenu">
                                        <!-- submenu -->
                                        <ul class="sub">
                                            <li>
                                                <a href="/certificacoes">
                                                    Certificações
                                                </a>    
                                            </li>
                                            <li>
                                                <a href="/nossa-historia">
                                                    Nossa História
                                                </a>
                                            </li>
                                            <li>
                                                <a href="onde-estamos">
                                                    Onde Estamos
                                                </a>
                                            </li>              
                                        </ul>
                                    </div>
                                </li>

                                <li class="sidebar-dropdown sidebar-dropdown__haschild animated animated__child">
                                    <a href="/produtos" class="hover hover-1">
                                        Produtos
                                    </a>
                                    <span></span>
                                    <div class="sidebar-submenu">
                                        <ul class="sub">
                                            <li>
                                                <a href="/implantes-mamarios">
                                                    Implates Mamários
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/implantes-de-contorno-corporal">
                                                    Implantes de Contorno Corporal
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/implantes-faciais-de-mento">
                                                    Implantes Faciais (de mento)
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/implantes-da-linha-de-urologia">
                                                    Implantes de Linha de Urologia
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/modelador-nasal">
                                                    Modelador Nasal
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/expansores-de-tecido">
                                                    Expansores de Tecido
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/medgel">
                                                    Medgel                                    
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/balao-gastrico">
                                                    Balão Gástrico
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                                <li class="sidebar-dropdown sidebar-dropdown__haschild animated animated__child">
                                    <a href="pedidos-de-compra" class="hover hover-1">
                                        Pedidos de Compra
                                    </a>
                                    <span></span>
                                    <div class="sidebar-submenu">
                                        <ul class="sub">
                                            <li>
                                                <a href="/para-medicos">
                                                    Para Médicos
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/para-pacientes">
                                                    Para Pacientes
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                                <li class="sidebar-dropdown animated">
                                    <a href="/estudo-clinico" class="hover hover-1">
                                        Estudo Clínico
                                    </a>
                                    <span></span>
                                </li>

                                <li class="sidebar-dropdown sidebar-dropdown__haschild animated animated__child">
                                    <a href="/pacientes" class="hover hover-1">Pacientes</a>
                                    <span></span>
                                    <div class="sidebar-submenu">
                                        <ul class="sub">
                                            <li>
                                                <a href="/seguranca-para-voce">
                                                    Cadastre-se
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/pacientes#psps">
                                                    PSPS
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                                <li class="sidebar-dropdown animated">
                                    <a href="/carreiras" class="hover hover-1">
                                        Carreiras
                                    </a>
                                    <span></span>
                                </li>

                                <li class="sidebar-dropdown animated">
                                    <a href="/contato" class="hover hover-1">
                                        Contato
                                    </a>
                                    <span></span>
                                </li>
                            </ul>
                        </div> 
                    </div>
                </nav>
            </div>

        </div><!-- banner menu classe -->
    </header> 
<!-- 
    html e body fecham no footer
-->