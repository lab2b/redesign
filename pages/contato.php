<main data-controller="contato">

    <div class="banner-container">                                                   
        <div class="banner-top" >
            <div class="bkgd-overlay"></div>
            <img src="./assets/imagem/banner/contato/banner.jpg"  alt="" srcset="">                            
            <div class="text-banner "  data-aos="fade-down">
                <h2>Contato</h2>
                <p>
                    Este é seu canal de comunicação com a SILIMED
                </p>
            </div>
            <div class="banner-animation-scrolldown">
                <div class="banner-line"></div>
            </div>
        </div>        
    </div>

    <section id="formContato">
        <div class="container">
            <div class="form-card">
                    <div class="row">
                    <div class="col-md-5 col-sm-12">
                        <div class="desc-form">
                            <h2>
                                Silimed Indústria de Implantes Ltda.
                            </h2>
                            <p>
                                Rua Figueiredo Rocha, 374  Rio de Janeiro • RJ • Brasil • CEP: 21240 - 660 Tel: + 55 (21) 3687-7000
                            </p>
    
                            <h2>
                                SAC - Serviço de Atendimento ao Cliente
                            </h2>
                            <p>
                            Horário de atendimento de Seg. a Sex. 09h às 18h 0800 9424199 assessoria@silimed.com.br
                            </p>
    
                            <h2>
                                Atendimento comercial
                            </h2>
                            <p>
                                0800 603 1500
                            </p>
                            <ul class="contact-social-links">
                                <li>
                                    <a href="http://">
                                        <span  data-toggle="tooltip" data-placement="top" title="Facebook">
                                            <i class="fab fa-facebook-f"></i>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://">
                                        <span  data-toggle="tooltip" data-placement="top" title="Instagram">
                                            <i class="fab fa-instagram"></i>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://">
                                        <span  data-toggle="tooltip" data-placement="top" title="Linkedin">
                                            <i class="fab fa-linkedin"></i>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-12">
                        <div class="form-content">                                
                            <form class="row" id="formContato">
                                <div class="col-lg-6 col-md-12">
                                    <label for="inputName" class="form-label"></label>
                                    <input type="text" class="form-control" id="inputName" placeholder="Nome">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label for="inputEmail" class="form-label"></label>
                                    <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label for="inputTelephone" class="form-label"></label>
                                    <input type="text" class="form-control" id="inputTelephone" placeholder="Telefone">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label for="inputCity" class="form-label"></label>
                                    <input type="text" class="form-control" id="inputCity" placeholder="Cidade">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label for="inputState" class="form-label"></label>
                                    <input type="text" class="form-control" id="inputState" placeholder="Estado">
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label for="inputRole" class="form-label"></label>
                                    <select id="inputRole" class="form-select">
                                        <option selected>Escolha uma opção</option>
                                        <option>Paciente</option>
                                        <option>Médico</option>
                                    </select>
                                </div>
                                <div class="col-12">
                                    <label for="inputCRM" class="form-label"></label>
                                    <input type="text" class="form-control" id="inputCRM" placeholder="CRM">
                                </div>
                                <div class="col-12">
                                    <label for="inputSubject" class="form-label"></label>
                                    <input type="text" class="form-control" id="inputSubject" placeholder="Assunto">
                                </div>
                                <div class="col-12">
                                    <label for="inputMessage" class="form-label"></label>
                                    <textarea class="form-control" id="inputMessage" rows="5" placeholder="Mensagem"></textarea>
                                </div>
                                <div class="col-12">
                                    <div class="submit-content">
                                        <small id="emailHelp" class="form-text text-muted">*Seus dados não serão compartilhados.</small>
                                        <button type="submit" class="btn-silimed orangeBg btn-submit">Enviar Mensagem</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


</main>