
<main data-controller="produtos">

<div class="banner-container">                                               
        <div class="banner-top">
            <div class="bkgd-overlay"></div>
            <img src="./assets/imagem/banner/produtos/banner.jpeg" alt="" srcset="">                            
            <div class="text-banner">
                <h2 data-aos="fade-down">Produtos</h2>
            </div>
        </div>
        <div class="banner-animation-scrolldown">
            <div class="banner-line"></div>
        </div>
</div>

<section id="produtos" data-aos="fade-up">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="default-desc">
                    <p>
                        Os produtos Silimed possuem alto desempenho e são desenvolvidos para melhor reproduzirem a anatomia de cada região do corpo. Contamos com a mais completa linha de implantes de silicone para atender as demandas de diversas especialidades médicas.  As matérias-primas são de grau médico e biocompatíveis.
                    </p>                
                </div>
            </div>

            <div class="col-md-4 col-sm-12">
                <div class="card-prod">
                    <a href="http://">
                        <p>
                            Implantes Mamámarios
                        </p>
                    </a>
                </div>                
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="card-prod">
                    <a href="http://">
                        <p>
                            Implantes de Contorno Corporal
                        </p>
                    </a>
                </div>                
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="card-prod">
                    <a href="http://">
                        <p>
                            Implantes Faciais (de mento)
                        </p>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-12">
                <div class="card-prod">
                    <a href="http://">
                        <p>
                            Implantes da Linha de Urologia
                        </p>
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="card-prod">
                    <a href="http://">
                        <p>
                            Expansores de Tecido
                        </p>
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="card-prod">
                    <a href="http://">
                        <p>
                            Medegel (Tratamento de Cicatrizes)
                        </p>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-12">
                <div class="card-prod">
                    <a href="http://">
                        <p>
                            Balão Gástrico
                        </p>
                    </a>
                </div>
            </div>

        </div>
    </div>

</section>
</main>