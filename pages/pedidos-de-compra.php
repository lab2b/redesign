
<main data-controller="pedidoDeCompra">

    <div class="banner-container">                                                   
        <div class="banner-top">
            <div class="bkgd-overlay"></div>
            <img src="./assets/imagem/banner/pedido-de-compra/banner.jpg" alt="" srcset="">                            
            <div class="text-banner">
                <h2 data-aos="fade-down">Pedidos de Compra</h2>
            </div>
            <div class="banner-animation-scrolldown">
                <div class="banner-line"></div>
            </div>
        </div>        
    </div>


    <section id="pedido-de-compra">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="buy-desc">
                        <p>
                            A Silimed apresenta aqui um novo modelo automatizado de atendimento no mercado de medical devices, que irá revolucionar a forma de realizar sua compra de implantes de silicone. Se você reside nas regiões atendidas pelas Filiais <b> SP* </b> ou <b> RJ*</b>, já pode fazer seu pedido de compra diretamente na plataforma disponibilizada aqui, o que traz mais benefícios. Confira!
                        </p>                    
                    </div>
                    <div class="buy-affiliate">
                        <p>
                            * Filial SP (São Paulo Capital, Grande São Paulo, Baixada Santista e Vale do Paraíba)
                        </p>
                        <p>
                            * Filial RJ (Estado do Rio de Janeiro, exceto regiões Serrana, Centro-sul Fluminense e Médio Paraíba)
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 align align__end">
                    <div class="btn-silimed orangeBg">
                        <a href="http://">
                            Médicos
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 align">
                    <div class="btn-silimed orangeBg">
                        <a href="http://">
                            Pacientes
                        </a>
                    </div>
                </div>

            </div>
        </div>

    </section>
</main>