
<main data-controller="estudoClinico">

    <div class="banner-container">                                                   
        <div class="banner-top">
            <div class="bkgd-overlay"></div>
            <img src="./assets/imagem/banner/estudo-clinico/banner.jpg" alt="" srcset="">                            
            <div class="text-banner">
                <h2 data-aos="fade-down">Estudo Clinico</h2>
            </div>
            <div class="banner-animation-scrolldown">
                <div class="banner-line"></div>
            </div>
        </div>        
    </div>


    <section id="clinicalMarketing">        
        <div class="mkt-clinical-content">            
            <div class="container">
                <div class="row">                    
                    <div class="col-12">
                        <div class="default-desc default-desc--container">
                            <p>
                                Acompanhamento e controle pós-mercado de segurança e eﬁcácia de implantes
                            </p>
                            <p>
                                mamários com superfície texturizada e superfície revestida com espuma de poliuretano da
                            </p>
                            <p>
                                marca Silimed®.
                            </p>                      
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="clinicalScience">
        <div class="wrapper">
            <div class="row grid-reverse">
                <div class="col-md-6 col-sm-12 grid-positionA">
                    <div class="content-science" data-aos="fade-up">
                        <div class="default-simple-text">                        
                            <p>
                                A Silimed sempre investe em ciência para garantir a excelência na segurança e desempenho dos seus produtos para os clientes. Por isso, é a patrocinadora do maior estudo clínico independente realizado no Brasil com implantes mamários que tem como objetivo comprovar segurança e eﬁcácia.¹, ²
                            </p>
                            <p>
                                O estudo clínico, realizado no Rio de Janeiro, de Fase IV com acompanhamento de 10 anos, com cirurgiões plásticos renomados, contando com uma equipe multidisciplinar e atendendo aos mais rigorosos padrões regulatórios internacionais.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 grid-positionB">
                    <div class="img-science" >
                        <img src="./assets/imagem/banner/estudo-clinico/nossa-historia-v1.jpg" alt="" srcset="">        
                    </div>
                </div>
            </div>
        </div>      
    </section>


    <section id="aboutClinical">
        <div class="about-clinical">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="content-about-clinical" data-aos="fade-up">
                            <div class="default-simple-text">
                                <h2 class="title-align-center">
                                    SOBRE O ESTUDO
                                </h2>
                                <p>
                                    É necessária a participação de 1.052 mulheres, com 18 anos ou mais, que tenham recebido implante(s) mamário(s) com revestimento texturizado ou de espuma de poliuretano, por indicação de aumento primário ou secundário. Serão realizadas 4 avaliações no 1º ano (14, 60, 180 e 365 dias após a implantação) e depois 1 avaliação ao ano, até completar 10 anos, totalizando 13 consultas presenciais. Para veriﬁcar o estado de saúde das participantes e para orientação quanto ao anda-mento do estudo, contatos serão realizados por telefone ou e-mail.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="benefitClinical">
        <div class="clinical-advantage">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="content-advantage">
                            <div class="text-advantage" data-aos="fade-up">
                                <h2>
                                    BENEFÍCIOS PARA PARTICIPANTES
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="advantage-user" data-aos="fade-up">
                            <div class="title-user">
                                <h3>
                                    PACIENTES
                                </h3>
                            </div>
                            <ul>
                                <li>
                                    <span class="fas fa-square"></span> garantia de acompanhamento médico por 10 anos (4 consultas no 1º ano e 1 visita anual a partir do 2º ano);
                                </li>
                                <li>
                                    <span class="fas fa-square"></span> contribuição para o avanço cientíﬁco nos quesitos segurança e eﬁcácia de implantes mamários.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="advantage-user" data-aos="fade-up">
                            <div class="title-user">
                                <h3>
                                    CIRURGIÕES PLÁSTICOS
                                </h3>
                            </div>
                            <ul>
                                <li>
                                    <span class="fas fa-square"></span>citação nominal em periódicos pela colaboração no estudo;
                                </li>
                                <li>
                                    <span class="fas fa-square"></span>possibilidade de acesso aos dados coletados para publicações científicas avaliadas e aprovadas pela Silimed;
                                </li>
                                <li>
                                    <span class="fas fa-square"></span>bonificação de um par de implantes para cada 10 pacientes comprovadamente incluídas no estudo;
                                </li>
                                <li>
                                    <span class="fas fa-square"></span>contribuição para o avanço científico nos quesitos segurança e eficácia de implantes mamários.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="infoContact">
        <div class="wrapper">
            <div class="row grid-reverse">
                <div class="col-md-6 col-sm-12 grid-positionA">
                    <div class="img-info" data-aos="zoom-in-right">
                        <img src="./assets/imagem/banner/estudo-clinico/nossa-historia-v2.jpg" alt="" srcset="">               
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 grid-postitionB">
                    <div class="content-infoContact" data-aos="fade-up">
                        <div class="text-infoContact">
                            <h2>
                                Para mais informações, entre em contato com:
                            </h2>
                        </div>
                        <div class="clinical-contact">
                            <h3>
                                Pesquisador Principal:
                            </h3>
                            <p>
                                Dr. Celso Eduardo Jandré Boechat
                            </p>
                            <p>
                                <span>Telefone:</span> (21) 99624-2604
                            </p>
                            
                            <p class="role">
                                Coordenadoras:
                            </p>
                            <p>
                                Veruska Artioli Michalski e Ana Luíza Santos
                            </p>
                            <p>
                                <span>Telefone: </span>(21) 99205-7005 e (21) 99208-0668
                            </p>
                            <p>
                                <span>E-mail: </span>estudoclinicostepsa@silimed.com.br
                            </p>
                        </div>
                        <div class="scientific-team">
                            <h3>
                                Equipe Científica:                        
                            </h3>
                            <p>
                                Dr. Pedro Emmanuel Alvarenga Americado do Brasil
                            </p>                        
                            <p class="role">
                                Médico Especialista -Epidemiologista
                            </p>

                            <p>
                                Dr. Henrique Pessoa Ladvocat Cintra                            
                            </p>                        
                            <p class="role">
                                Médico Especialista – Cirurgião Plástico
                            </p>

                            <p>
                                Ludmila Coelho Donato                            
                            </p>
                            <p>
                                Daniela da Silva Meneses Borba
                            </p>                        
                            <p class="role">
                                Monitoras de Pesquisa Clínica                            
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </section>


    <section id="clinicalReference">        
        <div class="reference-content">            
            <div class="container">
                <div class="row">                    
                    <div class="col-12">
                        <div class="reference-desc">
                            <h4>
                                Referência:
                            </h4>
                            <p>
                                1. Foi utilizada como fonte para a busca de estudos clínicos, o site de registro brasileiro de ensaios clínicos. E a busca realizada foi feita com as seguintes palavras chaves: Breast Implant, Polyurethane, primary augmentation, texture surface, safety and efﬁcacy implant, breast augmentation.        
                            </p>
                            <p>
                                2. Foi utilizada como fonte para a busca de estudos clínicos, a plataforma de registro “Clinical Trials”. E a busca realizada foi feita com as seguintes palavras chaves: Breast Implant/Status: Recruiting; Active not recruiting.
                            </p>             
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


</main>