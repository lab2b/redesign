<main data-controller="linhaStandard">

    <div class="banner-container">                                                   
        <div class="banner-top">
            <div class="bkgd-overlay"></div>
            <img src="./assets/imagem/banner/subpages/implantes-mamarios/banner.jpg" alt="" srcset="">                            
            <div class="text-banner">
                <h2 data-aos="fade-down">Linha Standard</h2>
            </div>
            <div class="banner-animation-scrolldown">
                <div class="banner-line"></div>
            </div>
        </div>        
    </div>
    <section id="classLine">
        <div class="wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="default-desc">
                        <p>
                            Essa linha é uma opção clássica de implantes mamários redondos.
                        </p>
                    </div>
                </div>
            </div>        
        </div>    
    </section>

    <section id="expansoresValvulas" class="orangeBg">
        <div class="wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="valvula-title">
                        <h2>
                            Tipos de válvulas remotas
                        </h2>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="valvula-area flex-display">
                        <div class="valvula-img" data-aos="fade-up">
                            <img src="./assets/imagem/banner/subpages/expansores-de-tecido/example-a.png" alt="" srcset=""> 
                        </div>
                        <div class="valvula-desc">
                            <h4>
                                VÁLVULA REDONDA
                            </h4>
                            <div class="paragraph-area" data-aos="fade-up">
                                <p>
                                    Possui um disco de aço inox interno para evitar o transpasse da agulha e
                                </p>
                                <p>
                                    facilitar a localização por radiografia.
                                </p>
                                <p>
                                    Diâmetro da base: 34mm (adulto); 22mm (infantil) Altura: 11mm (adulto); 8mm (infantil)
                                </p>                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-12">
                    <div class="valvula-area flex-display">
                        <div class="valvula-img" data-aos="fade-up">
                            <img src="./assets/imagem/banner/subpages/expansores-de-tecido/example-b.png" alt="" srcset=""> 
                        </div>
                        <div class="valvula-desc">
                            <h4>
                                LUER LOCK
                            </h4>
                            <div class="paragraph-area" data-aos="fade-up">
                                <p>
                                    Válvula com encaixe roscado para conexão direta na seringa sem agulha. 
                                </p>
                                <p>
                                    Possui um sistema de vedação que impede a saída de líquido 
                                </p>
                                <p>
                                    automaticamente ao desconectar a seringa.
                                </p>
                            </div>
                        </div>                                
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>    

