
<main data-controller="pacientes">

    <div class="banner-container">                                                   
        <div class="banner-top">
            <div class="bkgd-overlay"></div>
            <img src="./assets/imagem/banner/pacientes/banner.jpg" alt="" srcset="">                            
            <div class="text-banner">
                <h2 data-aos="fade-down">Pacientes</h2>
            </div>
            <div class="banner-animation-scrolldown">
                <div class="banner-line"></div>
            </div>
        </div>        
    </div>

    <section id="quality">
        <div class="wrapper">
            <div class="row">
                <div class="col-md-7 col-sm-12 px-0 orangeBg">
                    <div class="content-quality" data-aos="fade-up">
                        <div class="default-simple-text">                        
                            <p>
                                A Silimed é reconhecida pela qualidade e segurança dos produtos, pela parceria com os cirurgiões plásticos e por produzir produtos que atendem às necessidades individuais de pacientes.
                            </p>
                            <p>
                                O interesse maior da Silimed é oferecer satisfação e bem-estar aos clientes e, por isso, traz inovação, como a impressão pioneira do número de série em cada implante, facilitando a rastreabilidade dos produtos.&nbsp; Além disso, proporciona benefícios aos clientes, como o Programa de Substituição de Produto Silimed (PSPS).
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12 px-0 orangeBg">
                    <div class="img-quality" >
                        <img src="./assets/imagem/banner/pacientes/paciente-v1.jpg" alt="" srcset=""> 
                    </div>
                </div>
            </div>
        </div>      
    </section>
    
    <section id="psps">
        <div class="psps-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="psps-content">
                            <h2>
                                Programa de Substituição de Produto Silimed (PSPS)
                            </h2>
                            <p>
                                A escolha pela Silimed irá proporcionar às pacientes tranquilidade em adquirir implantes mamários seguros e de qualidade por oferecer um Programa de Substituição de Produto Silimed (PSPS) em casos de ruptura por defeito de fabricação ou por contratura capsular graus Baker III ou IV.
                            </p>
                            <p>
                                Após sua colocação, os implantes de silicone mamários são naturalmente envolvidos por uma cápsula tecidual, o que é uma reação normal do organismo. Em algumas pacientes, esta cápsula se torna vigorosa o suficiente para endurecer a mama e até modificar sua forma, conferindo-lhe um formato antiestético, endurecido e, às vezes, doloroso. Trata-se, portanto, de uma manifestação reativa do organismo à presença de um corpo estranho, isolando-o através da camada tecidual formada ao seu entorno, conhecida como cápsula fibrosa ou cápsula fibrótica. Essa reação do organismo é conhecida como contratura capsular. Existem quatro graus dela, de acordo com classificação de Baker, e a cobertura de nosso programa contempla os graus III e IV.
                            </p>
                            <div class="btn-silimed btn-silimed__info orangeBg">
                                <a href="/">
                                    <i class="far fa-file-alt"></i>
                                    Para mais informações sobre o psps, clique aqui
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="faq" data-aos="fade-up">
                            <div class="title-faq">
                                <h3>
                                    Perguntas Ferquentes
                                </h3>
                            </div>
                            <div class="accordion-faq">
                                <div id="accordion">
                                    
                                    <div class="card">
                                        <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                            <h5 class="mb-0">
                                                O que é o PSPS?
                                            </h5>
                                            <i class="icon-plus fas fa-plus"></i>
                                            <i class="icon-minus fas fa-minus"></i>
                                        </div>
                                        
                                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                            <div class="card-body">
                                                <p>
                                                    <span>
                                                        PSPS
                                                    </span>
                                                    é o Programa de Substituição de Produtos Silimed , um programa de reposição de produto em casos de ruptura de implantes mamários por defeito de fabricação ou por contratura capsular graus Baker III ou IV.
                                                </p>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <h5 class="mb-0">
                                                    O que seria Contratura Capsular?
                                            </h5>
                                            <i class="icon-plus fas fa-plus"></i>
                                            <i class="icon-minus fas fa-minus"></i>
                                        </div>
                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                            <div class="card-body">
                                                <p>
                                                    Após sua colocação, os implantes de silicone são naturalmente envolvidos por uma cápsula tecidual, o que é uma reação normal do organismo. Em alguns pacientes, esta cápsula se torna vigorosa o suficiente para endurecer a mama e até modificar sua forma, conferindo-lhe um formato antiestético, endurecido e, às vezes, doloroso. Essa reação do organismo é conhecida como contratura capsular.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                <h5 class="mb-0">
                                                    Como funciona o PSPS?
                                                </h5>
                                            <i class="icon-plus fas fa-plus"></i>
                                            <i class="icon-minus fas fa-minus"></i>
                                        </div>
                                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                            <div class="card-body">
                                                <p>
                                                    A Silimed fará a reposição de implantes mamários preenchidos com gel de silicone e revestidos nas superfícies texturizada (TRUE TEXTURE) ou poliuretano (PURE POLYURETHANE) nas seguintes situações:
                                                </p>
                                                <p>
                                                    –  Contratura Capsular                             
                                                </p>
                                                <p>
                                                    Embora seja uma manifestação natural do organismo, será feita a reposição nos casos de contratura capsular graus Baker III e IV para a cirurgia primária de aumento ou para a primeira cirurgia de reconstrução, considerando os seguintes períodos contados a partir da data da cirurgia:
                                                </p>
                                                <ul>
                                                    <li>
                                                        POLIURETANO: 10 anos 
                                                    </li>
                                                    <li>
                                                        TEXTURIZADO: Período de até 10 anos para cirurgias realizadas a partir de 03/05/2017 e de até 6 anos para cirurgias realizadas entre 01/09/2014 e 03/05/2017.
                                                    </li>
                                                </ul>
                                                <p>
                                                    –  Ruptura de implantes por defeito de fabricação, com reposição vitalícia nos casos de comprovada ruptura de implantes mamários por defeito de fabricação.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingFour" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            <h5 class="mb-0">
                                                Quais documentos são necessários para solicitar o PSPS?
                                            </h5>
                                            <i class="icon-plus fas fa-plus"></i>
                                            <i class="icon-minus fas fa-minus"></i>
                                        </div>
                                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                            <div class="card-body">
                                                <ul>
                                                    <li>
                                                        Exames de imagem (ultrassonografia, tomografia ou ressonância magnética) 
                                                    </li>
                                                    <li>
                                                        Relato médico carimbado e assinado explicando com detalhes o ocorrido 
                                                    </li>
                                                    <li>
                                                        Nota fiscal ou cartão do paciente que segue dentro da caixa do produto
                                                    </li>
                                                </ul>
                                                <p>
                                                    Todos os documentos devem estar legíveis.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingFive" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                                <h5 class="mb-0">
                                                    Em quais situações não teria direito de participar do PSPS?
                                                </h5>
                                            <i class="icon-plus fas fa-plus"></i>
                                            <i class="icon-minus fas fa-minus"></i>
                                        </div>
                                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                                            <div class="card-body">
                                                <ul>
                                                    <li>
                                                        Insatisfação com tamanho do implante ou resultado estético;                                     
                                                    </li>                                            
                                                    <li>
                                                        Ruptura do implante causada por outros motivos que não sejam referentes a defeito de fabricação, como por exemplo: ruptura causada durante a mamografia, entre outros;
                                                    </li>
                                                    <li>
                                                        Contratura Capsular grau Baker I ou II;                                    
                                                    </li>
                                                    <li>
                                                        Quando o evento de ruptura por defeito de fabricação ou contratura capsular Baker graus III ou IV não tiver como demonstração inicial exames/fotografias e laudo médico;                                    
                                                    </li>
                                                    <li>
                                                        Quando a paciente não apresentar cópia física do cartão da paciente fornecido durante ou após a cirurgia ou cópia da nota fiscal de compra;                                    
                                                    </li>
                                                    <li>
                                                        Em casos de cirurgias que não sejam primárias, ou seja, quando forem cirurgias de revisão de aumento, seroma, ou qualquer outro motivo de reoperação.                                    
                                                    </li>
                                                </ul>                                                                                                                                                                                        
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingSix" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                            <h5 class="mb-0">
                                                Como entrar em contato para sanar dúvidas?
                                            </h5>
                                            <i class="icon-plus fas fa-plus"></i>
                                            <i class="icon-minus fas fa-minus"></i>
                                        </div>
                                        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                                                <div class="card-body">
                                                    <p>
                                                        Caso você ainda tenha alguma dúvida entre em contato com a SILIMED através de nosso e-mail: assessoria@silimed.com.br ou 08009424199
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
    
                                </div>            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="pacientesCard">
        <div class="wrapper">
            <div class="row grid-reverse">                
                <div class="col-md-6 col-sm-12 grid-postitionA">
                    <div class="default-simple-text default-simple-text--gray" data-aos="fade-up">
                            <h2>
                                Cartão do Paciente                            
                            </h2>
                            <p>
                                Com objetivo de oferecer ainda mais segurança para os pacientes, cada caixa de implante Silimed é acompanhada por um cartão do paciente, onde o cirurgião plástico ou equipe fixa as etiquetas com o número de série, referência e volume de cada implante.                           
                            </p>
                            <p>
                                Por isso, esse cartão deve ser guardado pelo paciente que realizou a cirurgia com colocação de implante de silicone e deve ser apresentado por ele diante de exames de mamografia ou densitometria óssea mineral.                            
                            </p>
                            <p>
                                Caso o paciente deseje realizar uma cirurgia para a troca do implante de silicone, esse cartão deve ser apresentado ao cirurgião plástico para a escolha do novo implante.                           
                            </p>
                        </div>
                </div>
                <div class="col-md-6 col-sm-12 grid-positionB">
                    <div class="img-card" data-aos="zoom-in-left">
                        <img src="./assets/imagem/banner/pacientes/cartoes.png" alt="" srcset="">                                             
                    </div>
                </div>
            </div>
        </div>    
    </section>

    <section id="securityInfo">
        <div class="wrapper">
            <div class="row grid-reverse">                
                <div class="col-md-6 col-sm-12 grid-postitionA">
                    <div class="security-text">
                        <h2>
                            Segurança                         
                        </h2>
                        <p>
                            Visando sempre a segurança e o bem-estar de nossos clientes, a Silimed atua sempre com transparência, fornecendo informações importantes para o conhecimento dos pacientes.                         
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 grid-positionB align align__center">
                    <div class="btn-silimed  grayBg btn-security">
                        <a href="/">
                            Confira na íntegra
                        </a>
                    </div>
                </div>
            </div>
        </div>    
    </section>

    <section id="SingUpPatient">
        <div class="wrapper">
            <div class="row grid-reverse">
                <div class="col-md-6 col-sm-12 grid-positionA">
                    <div class="img-signUp" data-aos="zoom-in-right">
                        <img src="./assets/imagem/banner/pacientes/paciente-v2.jpg" alt="" srcset="">               
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 grid-postitionB ">
                    <div class="content-signUp">
                        <div class="text-signUp">
                            <h2>
                                Registro de Paciente
                            </h2>
                            <p>
                                <span>
                                    Os produtos da Silimed são sempre monitorados, mesmo após sua comercialização.
                                </span>
                            </p>
                            <p>
                                Se você adquiriu um implante Silimed, preencha os seus dados como paciente, as informações do produto e da cirurgia para que possamos ter seu registro atualizado em nosso cadastro e melhor atender vocês, pacientes.
                            </p>
                            <p>
                                Lembramos de solicitar ao seu Cirurgião Plástico após a cirurgia o cartão paciente que é enviado na embalagem do implante.
                            </p>
                        </div>
                        <div class="btn-silimed orangeBg ">
                            <a href="/">
                                Registre-se aqui
                            </a>
                        </div>
    
                    </div>
                </div>
            </div>
        </div>    
    </section>

</main>