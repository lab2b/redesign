<main data-controller="carreiras">

    <div class="banner-container">                                                   
        <div class="banner-top">
            <div class="bkgd-overlay"></div>
            <img src="./assets/imagem/banner/carreiras/banner.jpg" alt="" srcset="">                            
            <div class="text-banner">
                <h2 data-aos="fade-down">Carreiras</h2>
            </div>
            <div class="banner-animation-scrolldown">
                <div class="banner-line"></div>
            </div>
        </div>        
    </div>

    <section id="aboutCareer">
        <div class="wrapper">
            <div class="row grid-reverse">
                <div class="col-md-6 col-sm-12 grid-positionA">
                    <div class="content-career"  data-aos="fade-up">
                        <div class="default-simple-text">
                            <p>
                                Somos o maior fabricante de implantes de silicone na América Latina. Realizamos milhões sonhos e oferecemos saúde e bem-estar aos nossos clientes.
                            </p>
                            <p>
                                Para continuarmos sendo referência na fabricação de nossos produtos, precisamos de profissionais apaixonados pelo que fazem, que trabalham para oferecer o seu melhor, buscam resultados e são comprometidos em entregar produtos que realizam sonhos.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 grid-postitionB">
                    <div class="img-career" >
                        <img src="./assets/imagem/banner/carreiras/sobre.jpg" alt="" srcset=""> 
                    </div>
                </div>
            </div>
        </div>      
    </section>

    <section id="cultureCareer">
        <div class="wrapper">
            <div class="row grid-reverse">
                <div class="col-md-4 col-sm-12 grid-positionA">
                    <div class="content-values"  data-aos="fade-up">
                        <div class="default-simple-text">
                            <h2>
                                Nossa cultura
                            </h2>
                            <p>
                                Trabalhamos, apoiando e valorizando a equipe Silimed, para trazer ainda mais                                                                                qualidade aos nossos produtos. Por isso,                                                                                estabelecemos os seguintes Valores e Princípios.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12 grid-postitionB px-0">
                    <div class="img-values" >
                        <img src="./assets/imagem/banner/carreiras/infografico.jpg" alt="" srcset=""> 
                    </div>
                </div>
            </div>
        </div>      
    </section>



    <section id="work">
        <div class="wrapper">
            <div class="row">
                <div class="col-md-7 col-sm-12 bg-work">
                    <div class="img-x">
                        <!-- <img src="./assets/imagem/banner/carreiras/infografico.jpg" alt="" srcset="">  -->
                    </div>
                </div>
                <div class="col-md-5 col-sm-12">
                    <div class="content-work"  data-aos="fade-up">
                        <div class="default-simple-text">
                            <h2>
                                Trabalhe Conosco
                            </h2>
                            <p>
                                Se você tem esse foco e determinação, a Silimed tem a oportunidade para você.                                                                                qualidade aos nossos produtos. Por isso,                                                                                estabelecemos os seguintes Valores e Princípios.
                            </p>
                            <p class="upper">
                                FAÇA PARTE DA EQUIPE SILIMED.
                            </p>
                        </div>
                        <div class="btn-silimed orangeBg">
                            <a href="/">
                                Cadastre seu currículo aqui
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </section>


</main>    