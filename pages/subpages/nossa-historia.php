<main data-controller="nossaHistoria">

    <div class="banner-container">                                                   
        <div class="banner-top">
            <div class="bkgd-overlay"></div>
            <img src="./assets/imagem/banner/estudo-clinico/nossa-historia-v1.jpg" alt="" srcset="">                            
            <div class="text-banner">
                <h2 data-aos="fade-down">Nossa História</h2>
            </div>
            <div class="banner-animation-scrolldown">
                <div class="banner-line"></div>
            </div>
        </div>        
    </div>

    <section id="history-paragraph">
        <div class="wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="default-desc">
                        <p>
                            Onde você estava em 1978?
                        </p>
                        <p>
                            A Silimed já pensava no bem-estar dos seus pacientes.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="history-desc">
        <div class="wrapper">
            <div class="row grid-reverse">
                <div class="col-md-6 col-sm-12 grid-positionA px-0">
                    <div class="content-history-desc">
                        <div class="history-desc-text">
                            <h3>
                                HÁ MAIS DE 40 ANOS CONECTANDO CIÊNCIA E BEM-ESTAR.
                            </h3>
                            <p>
                                Presente no mercado desde 1978, a SILIMED é a maior fabricante de implantes de gel de silicone da América Latina, está presente em vários países e é líder em vendas no mercado brasileiro.
                            </p>
                            <p>
                                Por isso, há mais de 40 anos tem por vocação contribuir com a cirurgia plástica, promovendo pesquisas, estudos científicos e desenvolvendo as melhores tecnologias, sempre em parceria com Cirurgiões Plásticos.
                            </p>
                            <p>
                                Todos os produtos da Silimed são fabricados com matéria-prima de primeira qualidade e tecnologia de ponta, o que garante todas as certificações necessárias para atuação em diversos países.
                            </p>
                            <p>
                                A Silimed projeta um futuro de crescimento mundial e liderança nos diversos mercados onde atua, sem perder sua essência, conectando cada vez mais ciência e bem-estar.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 grid-postitionB px-0">
                    <div class="img-history">
                        <img src="./assets/imagem/banner/subpages/nossa-historia/nossa-historia-1.jpg" alt="" srcset=""> 
                    </div>
                </div>
            </div>
        </div>      
    </section>
    <!-- banner -->
    <section id="banner-timeline-1">
        <div class="wrapper">
            <div class="row">
                <div class="col-12 px-0">
                    <div class="img-timeline">
                        <img src="./assets/imagem/banner/subpages/nossa-historia/nossa-historia-full-1.jpg" alt="" srcset=""> 
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="text-timeline-1">
        <div class="wrapper">
            <div class="row grid-reverse">
                <div class="col-md-7 col-sm-12 grid-positionA px-0">
                    <div class="content-timeline">

                        <div class="timeline">

                            <div class="container-timeline " data-aos="flip-up">
                                <div class="content" data-aos="flip-up">
                                    <div class="year-timeline">
                                        1978
                                    </div>
                                    <p>
                                        Início das atividades Silimed no Rio de Janeiro
                                    </p>
                                </div>
                            </div>

                            <div class="container-timeline " data-aos="flip-up">
                                <div class="content" data-aos="flip-up">
                                    <div class="year-timeline">
                                        1981
                                    </div>
                                    <p>
                                        Começo da fabricação dos implantes de silicone no Brasil.
                                    </p>
                                </div>
                            </div>
                            
                            <div class="container-timeline " data-aos="flip-up">
                                <div class="content" data-aos="flip-up">
                                    <div class="year-timeline">
                                        1982
                                    </div>
                                    <p>
                                        Início da exportação e atuação no mercado internacional.
                                    </p>
                                </div>
                            </div>


                            <div class="container-timeline " data-aos="flip-up">
                                <div class="content" data-aos="flip-up">
                                    <div class="year-timeline">
                                        1984
                                    </div>
                                    <p>
                                        Começo da distribuição na América Latina de implantes mamários revestido com poliuretano, importados dos EUA.
                                    </p>
                                </div>
                            </div>

                            <div class="container-timeline " data-aos="flip-up">
                                <div class="content" data-aos="flip-up">
                                    <div class="year-timeline">
                                        1984
                                    </div>
                                    <p>
                                        Começo da distribuição na América Latina de implantes mamários revestido com poliuretano, importados dos EUA.
                                    </p>
                                </div>
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12 grid-postitionB px-0">
                    <div class="desc-timeline">
                        <h3>
                            <span>
                                1978-1988
                            </span> 
                            <br>
                            Início de um legado
                        </h3>
                        <p>
                            Em 1978, um grupo de jovens com visão de futuro resolveu unir o idealismo ao espírito empreendedor, investindo na criação de um novo negócio no mercado brasileiro. Nascia, no Rio de Janeiro, a Silimed, uma empresa que tinha no seu DNA prover saúde e bem-estar.
                        </p>
                        <p>
                            As atividades começaram no Brasil através da importação de implantes mamários da França. Em seguida, em 1981, começou produzir seus próprios implantes e no ano seguinte começou a exportar para Argentina. Os primeiros 10 anos foram marcados por desafios importantes para uma Indústria Brasileira que, em parceria com a classe médica, começou a se fortalecer em seu país e na América Latina.                        
                        </p>
                    </div>
                </div>



            </div>


            
        </div>      
    </section>


    <section id="banner-timeline-2">
        <div class="wrapper">
            <div class="row">
                <div class="col-12 px-0">
                    <div class="img-timeline">
                        <img src="./assets/imagem/banner/subpages/nossa-historia/nossa-historia-full-2.jpg" alt="" srcset=""> 
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="text-timeline-2">
        <div class="wrapper">
            <div class="row grid-reverse">
                <div class="col-md-5 col-sm-12 grid-positionA px-0">
                    <div class="desc-timeline">
                        <h3>
                            <span>
                                1988-1998
                            </span> 
                            <br>
                            CONSTRUÇÃO DE UM SONHO
                        </h3>
                        <p>
                            Em 1989 desenvolveu uma tecnologia própria e inovadora para produção de implantes mamários revestidos com espuma de poliuretano vulcanizada. No mesmo ano, ampliou sua atuação no mercado e começou a exportar os seus produtos à Europa.                        
                        </p>
                        <p>
                            Tornou-se a primeira empresa no mundo a identificar seus produtos com um número de série único, garantindo controle, rastreabilidade e segurança. Um item adicional que foi seguido por outros fabricantes e requerido como forma de credibilidade para cirurgiões plásticos, pacientes e órgãos certificadores.                        
                        </p>
                    </div>
                </div>
                <div class="col-md-7 col-sm-12 grid-postitionB px-0">

                    <div class="content-timeline">

                        <div class="timeline">

                            <div class="container-timeline " data-aos="flip-up">
                                <div class="content" data-aos="flip-up">
                                    <div class="year-timeline">
                                        1989
                                    </div>
                                    <p>
                                        Começo da produção de implantes mamários revestidos com poliuretano e ampliação da exportação de seus produtos à Europa.
                                    </p>
                                </div>
                            </div>

                            <div class="container-timeline " data-aos="flip-up">
                                <div class="content" data-aos="flip-up">
                                    <div class="year-timeline">
                                        1990
                                    </div>
                                    <p>
                                        Início da produção de implantes mamários com superfície texturizada desenvolvidos com tecnologia própria.
                                    </p>
                                </div>
                            </div>
                            
                            <div class="container-timeline " data-aos="flip-up">
                                <div class="content" data-aos="flip-up">
                                    <div class="year-timeline">
                                        1993
                                    </div>
                                    <p>
                                        Pioneira na impressão do número de série individual em cada implante.
                                    </p>
                                </div>
                            </div>


                            <div class="container-timeline " data-aos="flip-up">
                                <div class="content" data-aos="flip-up">
                                    <div class="year-timeline">
                                        1995
                                    </div>
                                    <p>
                                        Conquista da certificação ISO 9002 com foco na aquisição da marca CE. Uma das primeiras fabricantes de implantes mamários no mundo a ter o selo de qualidade.
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>      
    </section>



    <section id="banner-timeline-3">
        <div class="wrapper">
            <div class="row">
                <div class="col-12 px-0">
                    <div class="img-timeline">
                        <img src="./assets/imagem/banner/subpages/nossa-historia/nossa-historia-full-3.jpg" alt="" srcset=""> 
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="text-timeline-3">
        <div class="wrapper">
            <div class="row grid-reverse">
                <div class="col-md-7 col-sm-12 grid-positionA px-0">
                    
                <div class="content-timeline">
                    <div class="timeline">

                    <div class="container-timeline " data-aos="flip-up">
                            <div class="content" data-aos="flip-up">
                                <div class="year-timeline">
                                    1998
                                </div>
                                <p>
                                    Conquista da marca CE e intensificação da exportação e venda de produtos para a Europa.
                                </p>
                            </div>
                        </div>

                        <div class="container-timeline " data-aos="flip-up">
                            <div class="content" data-aos="flip-up">
                                <div class="year-timeline">
                                    2003
                                </div>
                                <p>
                                    Inauguração da 2a fábrica para ampliação da área produtiva.
                                </p>
                            </div>
                        </div>
                        
                        <div class="container-timeline " data-aos="flip-up">
                            <div class="content" data-aos="flip-up">
                                <div class="year-timeline">
                                    2006
                                </div>
                                <p>
                                    Relançamento do modelo exclusivo Silimed de implante cônico revestido com poliuretano.
                                </p>
                            </div>
                        </div>


                        <div class="container-timeline " data-aos="flip-up">
                                <div class="content" data-aos="flip-up">
                                <div class="year-timeline">
                                    2008
                                </div>
                                <p>
                                    Lançamento da BioDesign Collection, linha ampla e sofisticada de implantes mamários.
                                </p>
                            </div>
                        </div>

                </div>
            </div>





                </div>
                <div class="col-md-5 col-sm-12 grid-postitionB px-0">
                    <div class="desc-timeline">
                        <h3>
                            <span>
                                1998-2008
                            </span> 
                            <br>
                            CRESCIMENTO E INOVAÇÃO
                        </h3>
                        <p>
                            Após duas décadas de crescimento constante, consolida-se como líder no mercado brasileiro e como um dos principais players do mundo.
                        </p>
                        <p>
                            Para atender a grande demanda, construiu, em 2003, uma nova fábrica, ampliando a capacidade fabril e trazendo novos recursos tecnológicos.
                        </p>
                        <p>
                            Investiu em duas importantes inovações de produto nessa década. Relançou, em 2006, o implante mamário com design cônico, um modelo exclusivo Silimed até os dias de hoje. Em 2008, lançou uma linha de implantes mamários com um conceito de design exclusivo para atender diferentes biotipos, a Biodesign Collection, uma matriz completa, com 5 formatos e para cada tamanho de base até 4 projeções.
                        </p>
                    </div>
                </div>
            </div>
        </div>      
    </section>


    <section id="banner-timeline-4">
        <div class="wrapper">
            <div class="row">
                <div class="col-12 px-0">
                    <div class="img-timeline">
                        <img src="./assets/imagem/banner/subpages/nossa-historia/nossa-historia-full-4.jpg" alt="" srcset=""> 
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="text-timeline-4">
        <div class="wrapper">
            <div class="row grid-reverse">
                <div class="col-md-5 col-sm-12 grid-positionA px-0">
                    <div class="desc-timeline">
                        <h3>
                            <span>
                                2008-2018
                            </span> 
                            <br>
                            CONQUISTAS E NOVOS DESAFIOS
                        </h3>
                        <p>
                            O início dessa década foi marcada pela grande conquista de ser a primeira fabricante de implantes não norte-americana a receber aprovação do FDA.
                        </p>
                        <p>
                            Na mesma década, o grupo de jovens que iniciou esse grande negócio reestrutura o comando da Silimed e, sem perder sua essência e valor de marca, elege um membro da família, Gabriel Robert, para comandar a companhia como CEO. Um novo modelo de gestão mais dinâmico e orientado para o futuro é implementado. Esse grupo passou a compor o conselho administrativo.
                        </p>
                        <p>
                            Ao completar 40 anos, um novo ciclo de desafios se iniciou, com a abertura de filiais Internacionais, a construção de uma nova fábrica e a busca por inovações tecnológicas.
                        </p>
                    </div>
                </div>
                <div class="col-md-7 col-sm-12 grid-postitionB px-0">
                    <div class="content-timeline">
                        <div class="timeline">
                            
                            <div class="container-timeline " data-aos="flip-up">
                                <div class="content" data-aos="flip-up">
                                    <div class="year-timeline">
                                        2012
                                    </div>
                                    <p>
                                        Aprovação do FDA, tornando-se a única fabricante não-americana a receber a certificação. Gabriel Robert assume como novo CEO da empresa.
                                    </p>
                                </div>
                            </div>

                            <div class="container-timeline " data-aos="flip-up">
                                <div class="content" data-aos="flip-up">
                                    <div class="year-timeline">
                                        2014
                                    </div>
                                    <p>
                                        Destaque na revista Forbes, na editoria Guia de Investimentos.
                                    </p>
                                </div>
                            </div>

                            <div class="container-timeline " data-aos="flip-up">
                                <div class="content" data-aos="flip-up">
                                    <div class="year-timeline">
                                        2017
                                    </div>
                                    <p>
                                        Inauguração da nova sede administrativa no Rio de Janeiro, reposicionamento da marca Silimed e retomada da fabricação de mais linhas de produtos.
                                    </p>
                                </div>
                            </div>

                            <div class="container-timeline " data-aos="flip-up">
                                <div class="content" data-aos="flip-up">
                                    <div class="year-timeline">
                                        2018
                                    </div>
                                    <p>
                                        Celebração dos 40 anos da Silimed. Abertura de filiais internacionais no México e na Colômbia. Início da construção da nova fábrica.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>      
    </section>


</main>    