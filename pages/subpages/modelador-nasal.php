<main data-controller="modeladorNasal">

    <div class="banner-container">                                                   
        <div class="banner-top">
            <div class="bkgd-overlay"></div>
                <img src="./assets/imagem/banner/subpages/modelador-nasal/banner.png" alt="" srcset="">                            
            <div class="text-banner text-banner__bigger">
                <h2 data-aos="fade-down">Modelador Nasal</h2>
            </div>
            <div class="banner-animation-scrolldown">
                <div class="banner-line"></div>
            </div>
        </div>        
    </div>
    <section id="modeladorProd">
        <div class="wrapper">
            <div class="row grid-reverse">
                <div class="col-md-7 col-sm-12 grid-positionA grayBg px-0">
                    <div class="default-simple-text flex-display" data-aos="fade-up">
                        <p>
                            Com o intuito de atender à demanda do mercado e às solicitações dos cirurgiões plásticos de todo mundo, a Silimed apresenta sua linha de Modelador Nasal.
                        </p>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12 grid-positionB whiteBg px-0">
                    <div class="modelador-img-area">
                        <div class="modelador-img modelador-img__produto" data-aos="zoom-in-left">
                            <img src="./assets/imagem/banner/subpages/modelador-nasal/modelador-nasal.png" alt="" srcset="">                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="modeladorDesign">
        <div class="wrapper">
            <div class="row grid-reverse">
                <div class="col-md-5 col-sm-12 grid-positionB px-0">
                    <div class="modelador-img-area">
                        <div class="modelador-img modelador-img__modelo flex-display" data-aos="fade-up">
                            <img src="./assets/imagem/banner/subpages/modelador-nasal/mulher.png" alt="" srcset="">                            
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-sm-12 grid-positionA px-0 orangeBg">
                    <div class="default-simple-text flex-display" data-aos="fade-up">
                        <p>
                            Empregando tecnologia de ponta, seu design exclusivo proporciona uma série de vantagens ao paciente como: a manutenção do septo na posição vertical,  apaga  a memória da cartilagem ectópica, possibilita  a respiração nasal em pós operatório imediato, não obstruindo o fluxo de ar. Possui fácil higienização e esterilização, facilitando o procedimento pós-operatório e proporcionando a manutenção de resultados constantes.                        
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="modeladorIndication">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="indication-text-area flex-display">
                        <div class="indication-text" data-aos="fade-up">
                            <h3>
                                O Modelador Nasal Silimed é indicado para:
                            </h3>
                            <ul>
                                <li>
                                    Rinosseptoplastias
                                </li>
                                <li>
                                    Rinosseptoqueiloplastias
                                </li>
                                <li>
                                    Pós-rinoplastias estéticas e reparadoras
                                </li>
                                <li>
                                    Rinoqueiloplastias primárias e secundárias (pacientes fissurados)
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>    