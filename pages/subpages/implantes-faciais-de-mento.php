<main data-controller="implantesFaciaisDeMento">

    <div class="banner-container">                                                   
        <div class="banner-top">
            <div class="bkgd-overlay"></div>
            <img src="./assets/imagem/banner/subpages/implantes-faciais-de-mento/banner.jpg" alt="" srcset="">                            
            <div class="text-banner text-banner__bigger">
                <h2 data-aos="fade-down">Implantes Faciais (de mento)</h2>
            </div>
            <div class="banner-animation-scrolldown">
                <div class="banner-line"></div>
            </div>
        </div>        
    </div>

    <section id="facialImpante">
        <div class="wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="default-desc">
                        <p>
                            A Silimed produz implantes faciais, como o Implante de Mento e o de Mento Anatômico, com design exclusivo que apresentam características (forma, dureza e superfície) e reproduzem a anatomia da área do rosto.
                        </p>
                        <p>
                            Com baixa taxa de complicação e cicatriz externa não aparente, os Implantes de Mento são indicados para cirurgias reconstrutoras e estéticas por melhorar o equilíbrio¹ e restaurar a juventude da linha mandibular².
                        </p>
                    </div>
                </div>
            </div>        
        </div>    
    </section>
    <section id="deMentoImplante">
        <div class="deMento" data-aos="fade-up">
            <div class="wrapper">
                <div class="row grid-reverse">
                    <div class="col-md-7 col-sm-12 grid-positionA">
                        <div class="default-simple-text">
                            <h2>
                            Implante de Mento
                            </h2>
                            <p>
                                É constituído de elastômero de silicone de grau médico e apresenta forma arredondada com perfurações para fixação do tecido conjuntivo.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-12 grid-positionB">
                        <div class="img-demento">
                            <img src="./assets/imagem/banner/subpages/implantes-faciais-de-mento/demento.png" alt="" srcset="">                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="deMentoImplanteAnatomico">
        <div class="deMento" data-aos="fade-up">
            <div class="wrapper">
                <div class="row grid-reverse">
                    <div class="col-md-5 col-sm-12 grid-positionA">
                        <div class="img-demento">
                            <img src="./assets/imagem/banner/subpages/implantes-faciais-de-mento/demento-anatomico.png" alt="" srcset="">                            
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-12 grid-positionB">
                        <div class="default-simple-text">
                            <h2>
                                Implante de Mento Anatômico
                            </h2>
                            <p>
                                É constituído de elastômero de silicone de grau médico, cobre a expansão lateral total do queixo e apresenta uma lingueta reforçada na sua face interna para a sutura de fixação.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="clinicalReference">        
        <div class="reference-content">            
            <div class="container">
                <div class="row">                    
                    <div class="col-12">
                        <div class="reference-desc">
                            <h4>
                                Referência:
                            </h4>
                            <p>
                                1. Vertical Incision Intraoral Silicone Chin Augmentation Behrad B. Aynehchi, MD1, David H. Burstein, MD1, Afshin Parhiscar, MD1, and Mark A. Erlich, MD1 .
                            </p>
                            <p>
                                2. Mittelman H, Spencer JR, Chrzanowski DS. Chin region: management of grooves and mandibular hypoplasia withalloplastic implants. Facial Plast Surg Clin North Am. 2007 Nov;15(4):445-60, vi. Review. PubMed PMID: 18005885. AYNEHCHI, Behard B.; Burstein, David H.; Parhiscar, Afshin; Erlich, Mark A.; Vertical incision intraoral silicone chin augmentation. Otolaryngology – Head and Neck Surgery, v. 146, n. 4, p.553-559, 2012. AP06447
                            </p>             
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>    