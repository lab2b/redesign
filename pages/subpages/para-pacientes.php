<main data-controller="paraPacientes">

    <div class="banner-container">                                                   
        <div class="banner-top">
            <div class="bkgd-overlay"></div>
            <img src="./assets/imagem/banner/subpages/para-medicos/banner.jpg" alt="" srcset="">                            
            <div class="text-banner">
                <h2 data-aos="fade-down">Para Pacientes</h2>
            </div>
            <div class="banner-animation-scrolldown">
                <div class="banner-line"></div>
            </div>
        </div>        
    </div>

    <section id="purchasePatient">
            <div class="wrapper">
                <div class="silimend-left-area">
                    <div class="row">   
                        <div class="col-lg-4 col-sm-12">
                            <h2 class="silimed-left-title">
                                Para Pacientes
                            </h2>
                        </div>
                        <div class="col-lg-8 col-sm-12">
                            <div class="silimed-left-text">
                                <p>
                                    Pacientes, residentes  residentes nas regiões atendidas pelas Filiais <span> SP*</span>  ou <span> RJ*</span>, já podem fazer seu pedido de compra diretamente pelo site Silimed, o que traz mais benefícios para eles. Confira!                                
                                </p>
                                <ul>
                                    <li>
                                        Maior autonomia, agilidade e segurança na elaboração do pedido para a Silimed, sem a necessidade de envio de e-mails, de mensagens de texto ou contatos telefônicos.
                                    </li>
                                    <li>
                                        Mais facilidade na disponibilização dos produtos escolhidos.    
                                    </li>
                                    <li>
                                        Rapidez na entrega dos produtos, garantindo que estejam disponíveis dentro do prazo da cirurgia.
                                    </li>
                                    <li>
                                        Acesso direto, agregando valor ao atendimento do médico.
                                    </li>
                                </ul>
                            </div>
                            <div class="buy-affiliate">
                                <p>
                                    * Filial SP (São Paulo Capital, Grande São Paulo, Baixada Santista e Vale do Paraíba)
                                </p>
                                <p>
                                    * Filial RJ (Estado do Rio de Janeiro, exceto regiões Serrana, Centro-sul Fluminense e Médio Paraíba)
                                </p>
                            </div>
                            <div class="align align__center">
                                <div class="btn-silimed orangeBg">
                                    <a href="/">
                                        Efetuar Pedido
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>

</main>    