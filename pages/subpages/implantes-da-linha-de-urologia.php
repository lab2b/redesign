<main data-controller="implantesDaLinhaDeUrologia">

    <div class="banner-container">                                                   
        <div class="banner-top">
            <div class="bkgd-overlay"></div>
                <img src="./assets/imagem/banner/subpages/implantes-da-linha-de-urologia/banner.jpg" alt="" srcset="">                            
            <div class="text-banner text-banner__bigger">
                <h2 data-aos="fade-down">Implantes da Linha de Urologia</h2>
            </div>
            <div class="banner-animation-scrolldown">
                <div class="banner-line"></div>
            </div>
        </div>        
    </div>

    <section id="descUrologia">
        <div class="wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="default-desc">
                        <p>
                            Os implantes testiculares são produtos da linha de Urologia Silimed que se destacam no mercado por seu design exclusivo, pelo alto perfil de segurança e por suas características como forma dureza e maleabilidade, que melhor reproduzem a anatomia das áreas de destino.
                        </p>
                    </div>
                </div>
            </div>        
        </div>    
    </section>

    <section id="implanTesticulares">
        <div class="wrapper">
            <div class="row grid-reverse">
                <div class="col-md-5 col-sm-12 grid-positionA px-0">
                    <div class="urologia-img-area urologia-img-area__testiculares" > <!--  data-aos="zoom-in-right" -->
                        <h2>
                            Implantes Testiculares
                        </h2>
                        <div class="urologia-img urologia-img__testiculares">
                            <img src="./assets/imagem/banner/subpages/implantes-da-linha-de-urologia/testiculares.jpg" alt="" srcset="">
                        </div>
                    </div>
                </div>

                <div class="col-md-7 col-sm-12 grid-positionB px-0 grayBg">
                    <div class="urologia-desc-area urologia-desc-area__testiculares" data-aos="fade-up">
                        <h2>
                            Indicações
                        </h2>
                        <h4>
                            NA CONSTRUÇÃO ESTÉTICA, EM CASOS DE:
                        </h4>
                        <ul>
                            <li>
                                Ausência de testículos;
                            </li>
                            <li>
                                Redesignação sexual;                            
                            </li>
                        </ul>
                        <h4>
                            NAS CIRURGIAS RECONSTRUTORAS, EM CASOS COMO:
                        </h4>
                        <ul>
                            <li>
                                Má formação congênita;
                            </li>
                            <li>
                                Traumatismos;
                            </li>
                            <li>
                                Afecções dos testículos (epididimite ou orquite);
                            </li>
                            <li>
                                Após câncer testicular ou de próstata;
                            </li>
                            <li>
                                Atrofias por traumas ou torção;
                            </li>
                        </ul>
                        <h2>
                            Diferenciais
                        </h2>
                        <div class="decoration-list">
                            <p>
                                <span class="fas fa-check-circle"></span>   Constituído de membrana de elastômero mecanicamente resistente.                     
                            </p>
                            <p>
                                <span class="fas fa-check-circle"></span>    Preenchido com gel de silicone 100% grau médico de alto desempenho.                    
                            </p>
                            <p>
                                <span class="fas fa-check-circle"></span>    Desenvolvido de forma que a densidade e consistência sejam similares ao tecido do testículo                    
                            </p>
                            <p>
                                <span class="fas fa-check-circle"></span>   Possui pastilhas para fixação para minimizar o deslocamento do implante                     
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="implanPenianos">
        <div class="wrapper">
            <div class="row grid-reverse">
                <div class="col-md-7 col-sm-12 grid-positionB px-0 orangeBg">
                    <div class="urologia-desc-area urologia-desc-area__penianos" data-aos="fade-up">
                        <h2>
                            Indicações
                        </h2>
                        <h4>
                            PARA CORREÇÃO DA DISFUNÇÃO ERÉTIL DEVIDO A VÁRIAS CAUSAS, COMO:
                        </h4>
                        <ul>
                            <li>
                                Prostatectomia;
                            </li>
                            <li>
                                Doença de peyronie;
                            </li>
                            <li>
                                Fibrose peniana;
                            </li>
                            <li>
                                Outras.
                            </li>
                        </ul>
                        
                        <h2>
                            Diferenciais
                        </h2>

                        <div class="decoration-list">
                            <p>
                                <span class="fas fa-check-circle"></span>   Confeccionado em elastômero de silicone
                            </p>
                            <p>
                                <span class="fas fa-check-circle"></span>   Constituído de extremidade proximal, corpo principal, extremidade distal, alma de prata (que orientará as posições de micção e de ereção) e 4 ponteiras extensoras de 5mm a 15mm
                            </p>
                            <p>
                                <span class="fas fa-check-circle"></span>   Desenvolvido de forma que a densidade e consistência sejam similares ao tecido do testículo
                            </p>
                            <p>
                                <span class="fas fa-check-circle"></span>  Disponibilizado com 2 diâmetros (9 e 11 mm) e em diferentes comprimentos que podem ser ajustados durante a implantação de acordo com os corpos cavernosos
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12 grid-positionA px-0">
                    <div class="urologia-img-area urologia-img-area__penianos" > <!-- data-aos="zoom-in-left" -->
                        <h2>
                            Implantes Penianos
                        </h2>
                        <div class="urologia-img urologia-img__penianos">
                            <img src="./assets/imagem/banner/subpages/implantes-da-linha-de-urologia/penianos.jpg" alt="" srcset="">
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>
</main>    