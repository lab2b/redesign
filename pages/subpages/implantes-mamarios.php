<main data-controller="implantesMamarios">
    <div class="banner-container">                                                   
        <div class="banner-top">
            <div class="bkgd-overlay"></div>
            <img src="./assets/imagem/banner/subpages/implantes-mamarios/banner.jpg" alt="" srcset="">                            
            <div class="text-banner">
                <h2 data-aos="fade-down">Implantes Mamários</h2>
            </div>
            <div class="banner-animation-scrolldown">
                <div class="banner-line"></div>
            </div>
        </div>        
    </div>

    <section id="objetivoProduto">
        <div class="wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="default-desc">
                        <p>
                            Um dos objetivos dos avanços tecnológicos da Silimed é criar implantes que ofereçam resultados cada vez mais adequados às necessidades das pacientes. Dessa maneira, a empresa inovou no mercado de implantes mamários ao lançar a linha BioDesign. Para uma opção mais clássica de implantes, apresentamos a linha Standard.
                        </p>
                    </div>
                </div>
            </div>        
        </div>    
    </section>

    <section id="productLine">
        <div class="wrapper">
            <div class="row">            
                <div class="col-md-6 col-sm-12 px-0">
                    <div class="line line--biodesign">
                        <h2 data-aos="fade-up">
                            Linha Biodesign
                        </h2>
                        <p data-aos="fade-up">
                            Com um conceito inovador, oferece ao cirurgião plástico a possibilidade de escolha do implante de silicone de acordo com o biotipo da paciente. Ele considera as medições da mama para definir a medida da base do implante.
                        </p>
                        <div class="btn-silimed btn-silimed__center grayBg" data-aos="fade-up">
                            <a href="/home" target="_blank" rel="noopener noreferrer">
                                Saiba Mais                        
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 px-0">
                    <div class="line line--standard">
                        <h2 data-aos="fade-up">
                            LINHA STANDARD
                        </h2>
                        <p data-aos="fade-up">
                            Uma opção clássica de implantes redondos que traz uma grande versatilidade de indicações e um preenchimento mais natural dos seios.
                        </p>
                        <div class="btn-silimed btn-silimed__center orangeBg" data-aos="fade-up">
                            <a href="/home" target="_blank" rel="noopener noreferrer" >
                                Saiba Mais                        
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="informationFolder">
        <div class="wrapper" data-aos="fade-up">
            <div class="row">
                <div class="col-md-7 col-sm-12">
                    <div class="text-mamario-area">
                        <p>
                            Confira aqui as respostas para as principais dúvidas sobre implantes mamários.
                        </p>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12">
                    <div class="btn-silimed btn-silimed__info grayBg">
                        <a href="/" target="_blank" rel="noopener noreferrer">
                            <i class="far fa-file-pdf"></i> Folheto Pacientes Implantes Mamários
                        </a>
                    </div>
                </div>
            </div>
            <div class="area-btn">
                <div class="row">                
                    <div class="col-12">
                        <div class="text-mamario-area">
                            <h4>
                                Para mais informações, acesse a instrução de uso do produto.
                            </h4>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="btn-silimed btn-silimed__info grayBg">
                            <a href="/" target="_blank" rel="noopener noreferrer">
                                <i class="far fa-file-pdf"></i> Implantes Mamário de poliuretano
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="btn-silimed btn-silimed__info grayBg">
                            <a href="/" target="_blank" rel="noopener noreferrer">
                                <i class="far fa-file-pdf"></i> Implantes Mamário texturizado
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="btn-silimed btn-silimed__info grayBg">
                            <a href="/" target="_blank" rel="noopener noreferrer">
                                <i class="far fa-file-pdf"></i> Implantes Mamário liso
                            </a>
                        </div>
                    </div>                                       
                </div>  
            </div>
        </div>    
    </section>
    <section id="areaVideo">
        <div class="wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="default-desc" data-aos="fade-up">
                        <p>
                            Acesse o 
                            <span>
                                Board Silimed 3D 
                            </span>
                            e visualize os perfis, os formatos 
                        </p>
                        <p>
                        e as projeções dos implantes mamários da
                            <span>
                            linha BioDesign
                            </span>
                        </p>
                    </div>
                </div>
            </div>        
        </div> 
        <div class="container">
            <div class="row">
                <div class="col-12" data-aos="fade-up">
                    <!-- item -->
                    <div class="sketchfab-embed-wrapper"> <iframe class="iframe-responsive" title="Board Silimed 3D" frameborder="0" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" allow="fullscreen; autoplay; vr" xr-spatial-tracking execution-while-out-of-viewport execution-while-not-rendered web-share width="1080" height="450" src="https://sketchfab.com/models/1f9891df7e2f4777b51e32419ab09b7c/embed"> </iframe> <p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;"> <a href="https://sketchfab.com/3d-models/board-silimed-3d-1f9891df7e2f4777b51e32419ab09b7c?utm_medium=embed&utm_campaign=share-popup&utm_content=1f9891df7e2f4777b51e32419ab09b7c" target="_blank" style="font-weight: bold; color: #1CAAD9;"> Board Silimed 3D </a> by <a href="https://sketchfab.com/silimed?utm_medium=embed&utm_campaign=share-popup&utm_content=1f9891df7e2f4777b51e32419ab09b7c" target="_blank" style="font-weight: bold; color: #1CAAD9;"> silimed </a> on <a href="https://sketchfab.com?utm_medium=embed&utm_campaign=share-popup&utm_content=1f9891df7e2f4777b51e32419ab09b7c" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a></p></div> 
                </div>   
    </section>

</main>    