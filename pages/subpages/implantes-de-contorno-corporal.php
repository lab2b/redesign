<main data-controller="implantesDeContornoCorporal">

    <div class="banner-container">                                                   
        <div class="banner-top">
            <div class="bkgd-overlay"></div>
            <img src="./assets/imagem/banner/subpages/implantes-de-contorno-corporal/banner.jpg" alt="" srcset="">                            
            <div class="text-banner text-banner__bigger">
                <h2 data-aos="fade-down">Implantes de Contorno Corporal</h2>
            </div>
            <div class="banner-animation-scrolldown">
                <div class="banner-line"></div>
            </div>
        </div>        
    </div>
    <section id="ModelosProduto">
        <div class="wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="default-desc">
                        <p>
                        Para atender às solicitações dos cirurgiões plásticos, que tem por vocação promover o bem-estar de seus pacientes, a Silimed oferece uma linha completa de implantes de contorno corporal, desenvolvida em parceria com renomados cirurgiões. São modelos exclusivos que se adaptam às regiões do glúteo e da panturrilha e simulam melhor os tecidos dessas áreas de destino. Esta linha de produtos se destaca no mercado pela tecnologia empregada e pelo alto perfil de segurança.
                        </p>
                    </div>
                </div>
            </div>        
        </div>    
    </section>
    <section id="gluteoProduto">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="gluteo-desc">
                        <div class="text-gluteo-desc">
                            <h2>
                                Implantes de Glúteo
                            </h2>
                            <p>
                                Somente os implantes glúteos Silimed possuem a perfeita combinação das dimensões da base e da projeção que se ajustam de forma harmônica à anatomia da região, proporcionando um resultado satisfatório para a paciente.
                            </p>
                            <p>
                                Esses produtos são produzidos com uma membrana de elastômero resistente, preenchidos com gel de silicone 100% grau médico e de alto desempenho. Por ser uma região que sofre impactos e pressões constantes, o gel possui consistência e dureza desenvolvidas para assemelhar-se aos tecidos da área e manter a resistência.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="gluteo-img-desc">
            <div class="wrapper">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="gluteo-prod-item">
                            <div class="gluteo-img">
                                <img src="./assets/imagem/banner/subpages/implantes-de-contorno-corporal/gluteo-quartzo.png" alt="" srcset="">                            
                            </div>
                            <div class="prod-item-desc">
                                <h4>
                                    GLÚTEO QUARTZO
                                </h4>
                                <p>
                                    Apresenta base oval, projeção alta e superfície lisa opaca, obtida por meio de tecnologia diferenciada. O implante Glúteo Quartzo é indicado para quem tem um formato de quadril, onde a altura é maior e a largura menor.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="gluteo-prod-item">
                            <div class="gluteo-img">
                                <img src="./assets/imagem/banner/subpages/implantes-de-contorno-corporal/gluteo-redondo.png" alt="" srcset="">                            
                            </div>
                            <div class="prod-item-desc">
                                <h4>
                                    GLÚTEO REDONDO
                                </h4>
                                <p>
                                    Apresenta base redonda com superfície lisa brilhante. Ideal para quem possui um formato de quadril, onde a altura e a largura são semelhantes.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7 col-sm-12 align align__center">
                        <div class="text-mamario-area">
                            <p>
                                Confira aqui as respostas para as principais dúvidas sobre implantes de glúteos.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-12">
                        <div class="btn-silimed btn-silimed__info grayBg">
                            <a href="/" target="_blank" rel="noopener noreferrer">
                                <i class="far fa-file-pdf"></i> Folheto Pacientes Implantes de glúteo
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="panturrilhaProduto">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="panturrilha-desc">
                        <div class="text-panturrilha-desc">
                            <h2>
                                Implantes de Panturrilha
                            </h2>
                            <p>
                                Desenhado para se aproximar mais da anatomia da região com base assimétrica, é preenchido com gel de silicone de alto desempenho e com a consistência ideal para a área destinada. É indicado para corrigir as desigualdades ou irregularidades das pernas.
                            </p>
                        </div>
                    </div>
                    <div class="panturrilha-img">
                        <img src="./assets/imagem/banner/subpages/implantes-de-contorno-corporal/panturrilha.png" alt="" srcset="">                            
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>    