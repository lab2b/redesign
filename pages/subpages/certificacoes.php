<main data-controller="certificacoes">

    <div class="banner-container">                                                   
        <div class="banner-top">
            <div class="bkgd-overlay"></div>
            <img src="./assets/imagem/banner/subpages/certificacoes/banner.jpg" alt="" srcset="">                            
            <div class="text-banner">
                <h2 data-aos="fade-down" >Certificações</h2>
            </div>
            <div class="banner-animation-scrolldown">
                <div class="banner-line"></div>
            </div>
        </div>        
    </div>

    <section id="certifications">
            <div class="wrapper">

                <div class="row">
                    <div class="col-12">
                        <div class="default-desc" >
                            <p>
                                Todos produtos da Silimed são fabricados com matéria-prima de qualidade e tecnologia de ponta, que garantem todas as certificações necessárias para sua atuação.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="certificate-silimed"  data-aos="fade-up">
                    <div class="row">   
                        <div class="col-lg-4 col-sm-12">
                            <h2 class="title-certificate">
                                Brasil
                            </h2>
                        </div>
                        <div class="col-lg-8 col-sm-12">
                            <p>
                                Instalações e processos de produção inspecionados e produtos registrados/cadastrados junto à Agência Nacional de Vigilância Sanitária (ANVISA), vinculado ao Ministério da Saúde, e certificados pelo Instituto Nacional de Tecnologia (INT), órgão credenciado pelo Instituto Nacional de Metrologia, Qualidade e Tecnologia (INMETRO). As condições ambientais e as técnicas produtivas são controladas pelas Boas Práticas de Fabricação para Produtos Médicos (BPF) da ANVISA.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="certificate-seal"  data-aos="fade-up">
                    <div class="row">
                        <div class="col-lg-4 col-sm-12">  </div>
                        <div class="col-lg-8 col-sm-12">
                            <ul class="seal-list">
                                <li>
                                    <div class="img-seal">
                                        <img src="./assets/imagem/banner/subpages/certificacoes/logo-anvisa.png" alt="" srcset=""> 
                                    </div>
                                </li>
                                <li>
                                    <div class="img-seal">
                                        <img src="./assets/imagem/banner/subpages/certificacoes/logo-inmetro.png" alt="" srcset=""> 
                                    </div>
                                </li>
                                <li>
                                    <div class="img-seal">
                                        <img src="./assets/imagem/banner/subpages/certificacoes/logo-int.png" alt="" srcset=""> 
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>

                <div class="certificate-silimed"  data-aos="fade-up">
                    <div class="row">   
                        <div class="col-lg-4 col-sm-12">
                            <h2 class="title-certificate">
                                Demais Países
                            </h2>
                        </div>
                        <div class="col-lg-8 col-sm-12">
                            <p>
                                Atuação em conformidade com todos os requisitos formais de cada ministério da saúde nos países onde seus produtos são distribuídos.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="certificate-silimed"  data-aos="fade-up">
                    <div class="row">   
                        <div class="col-lg-4 col-sm-12">
                            <h2 class="title-certificate">
                                ISO 13485
                            </h2>
                        </div>
                        <div class="col-lg-8 col-sm-12">
                            <p>
                                Conquista da ISO 13485, norma internacionalmente reconhecida como sistema de gestão da qualidade para a indústria de dispositivos médicos.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="certificate-seal"  data-aos="fade-up">
                    <div class="row">
                        <div class="col-lg-4 col-sm-12">
                            
                        </div>
                        <div class="col-lg-8 col-sm-12">
                            <ul class="seal-list">
                                <li>
                                    <div class="img-seal">
                                        <img src="./assets/imagem/banner/subpages/certificacoes/iso-13485.png" alt="" srcset=""> 
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
    </section>





</main>    