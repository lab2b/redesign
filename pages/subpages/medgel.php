<main data-controller="medgel">

    <div class="banner-container">                                                   
        <div class="banner-top">
            <div class="bkgd-overlay"></div>
                <img src="./assets/imagem/banner/subpages/medgel/banner.jpg" alt="" srcset="">                            
            <div class="text-banner text-banner__bigger">
                <h2 data-aos="fade-down">Medgel</h2>
                <h3 data-aos="fade-down">Tratamento de Cicatrizes</h3>                
            </div>
            <div class="banner-animation-scrolldown">
                <div class="banner-line"></div>
            </div>
        </div>        
    </div>
    <section id="medgelProd">
        <div class="wrapper">
            <div class="row grid-reverse">
                <div class="col-md-6 col-sm-12 grid-positionA px-0">
                    <div class="medgel-img" >
                        <img src="./assets/imagem/banner/subpages/medgel/medgel-produto-1.jpg" alt="" srcset="">                            
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 grid-positionB px-0 orangeBg flex-display" >
                    <div class="medgel-desc" data-aos="fade-up">
                        <h3>
                            O Medgel é um produto feito à base de gel de silicone 100% grau médico.
                        </h3>
                        <div class="decoration-list">
                            <p>
                                <span class="fas fa-check-circle"></span>  
                                Biocompatível
                            </p>
                            <p>
                                <span class="fas fa-check-circle"></span>  
                                Flexível        
                            </p>
                            <p>
                                <span class="fas fa-check-circle"></span>                
                                Reutilizável e com suave aderência
                            </p>
                            <p>
                                <span class="fas fa-check-circle"></span>    
                                Sem componentes de origem animal
                            <p>
                                <span class="fas fa-check-circle"></span>    
                                Sem princípio ativo
                            </p>
                            <p>
                                <span class="fas fa-check-circle"></span>    
                                Pode ser usado associado a outros tratamentos complementares
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="medgelAuthentic">
        <div class="wrapper">
            <div class="row grid-reverse">
                
                <div class="col-md-6 col-sm-12 grid-positionA px-0">
                    <div class="authentic-img">
                        <img src="./assets/imagem/banner/subpages/medgel/medgel-seal.png" alt="" srcset="">                            
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 grid-positionB px-0">
                    <div class="medgel-desc">
                        <h3>
                            Selo de autenticidade
                        </h3>
                        <p>
                            Todos os produtos Medgel* recebem a gravação da marca Silimed durante a produção. Com isso, a Silimed oferece mais segurança e garantia na entrega de um produto original.
                        </p>
                        <span>
                            *exceto Medgel Coat
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="medgelResult">
        <div class="wrapper">
            <div class="row grid-reverse">
                
                
                <div class="col-md-6 col-sm-12 grid-positionA px-0 grayBg">
                    <div class="medgel-result-desc flex-display">
                        <h3>
                            Melhora a aparência em até 8 semanas*
                        </h3>
                        <p>
                            Todos os produtos Medgel* recebem a gravação da marca Silimed durante a produção. Com isso, a Silimed oferece mais segurança e garantia na entrega de um produto original.
                        </p>
                        <span>
                            *exceto Medgel Coat
                        </span>
                    </div>
                </div>

                <div class="col-md-6 col-sm-12 grid-positionB px-0">
                    <div class="result-img">
                        <img src="./assets/imagem/banner/subpages/medgel/medgel-antes.jpg" alt="" srcset="">                            
                    </div>
                    <div class="result-img">
                        <img src="./assets/imagem/banner/subpages/medgel/medgel-depois.jpg" alt="" srcset="">                            
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <sections id="idications">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="linha-completa-area">
                        <div class="linha-completa-text flex-display" data-aos="fade-up">
                            <h2>
                                Indicações
                            </h2>
                            <p>
                                Auxilia no tratamento e na prevenção de quelóides, cicatrizes 
                            </p>
                            <p>
                                comuns e hipertróficas, recentes ou antigas, e associadas 
                            </p>
                            <p>
                                ao eritema (vermelhidão).
                            </p>
                        </div>
                    </div>
                    <div class="linha-completa-img">
                        <div class="produtos-img" data-aos="fade-up">
                            <img src="./assets/imagem/banner/subpages/medgel/medgel-produto-2.png" alt="" srcset="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper wrapper__px-30">
            <div class="linha-competa-itens">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <div class="medgel-item">
                            <h4>
                                Medgel Gel
                            </h4>
                            <p>
                                Indicado para cicatrizes amplas ou espalhadas, como por exemplo, provenientes de queimaduras.
                            </p>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-12">
                        <div class="medgel-item">
                            <h4>
                                Medgel Placa
                            </h4>
                            <p>
                                Indicado para cicatrizes amplas ou espalhadas, como por exemplo, provenientes de queimaduras.
                            </p>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-12">
                        <div class="medgel-item">
                            <h4>
                                Medgel Tira
                            </h4>
                            <p>
                                Indicado para cicatrizes lineares, como de abdominoplastia, mamoplastias, cesarianas, cirurgias cardíacas etc.
                            </p>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-12">
                        <div class="medgel-item">
                            <h4>
                                Medgel Disco
                            </h4>
                            <p>
                                Indicados para cicatrizes areolares, tanto de redução como de aumento mamário.
                            </p>
                        </div>
                    </div>

                    <div class="col-12 flex-display">
                        <div class="btn-silimed btn-silimed__center grayBg">
                            <a href="/"> 
                                Compre aqui
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </sections>

    <section id="medgelAction" class="grayBg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h4>
                        Mecanismo de ação¹
                    </h4>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="medgel-action-steps ">
                        <div class="img-steps flex-display" data-aos="fade-up">
                            <img src="./assets/imagem/banner/subpages/medgel/medgel-acao-1.jpg" alt="" srcset="">
                        </div>
                        <div class="steps-desc">
                            <h4>
                                FORMAÇÃO DA CICATRIZ - 1ª ETAPA
                            </h4>   
                            <p>
                                A estrutura normal das células consiste em estarem alinhadas e organizadas. Porém, quando ocorre uma lesão, um corte ou uma queimadura, este processo muda. A área afetada perde água excessivamente (1), o que provoca uma produção desordenada de colágeno e células de reparo, levando ao aparecimento da cicatriz (2)
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12">
                    <div class="medgel-action-steps">
                        <div class="img-steps flex-display" data-aos="fade-up">
                            <img src="./assets/imagem/banner/subpages/medgel/medgel-acao-2.jpg" alt="" srcset="">
                        </div>
                        <div class="steps-desc">
                            <h4>
                                AÇÃO DO MEDGEL - 2ª ETAPA
                            </h4>   
                            <p>
                                Com o auxílio do Medgel, o processo passa por uma organização e regeneração tecidual. Por meio da oclusão da fita de silicone (3), ocorre uma melhora na hidratação da pele, normalizando a produção de colágeno e das células de reparo.                            
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12">
                    <div class="medgel-action-steps">
                        <div class="img-steps flex-display" data-aos="fade-up">
                            <img src="./assets/imagem/banner/subpages/medgel/medgel-acao-3.jpg" alt="" srcset="">
                        </div>
                        <div class="steps-desc">
                            <h4>
                                RESULTADO DA AÇÃO - 3ª ETAPA
                            </h4>   
                            <p>
                                O resultado é uma pele com melhor reorganização do colágeno e consequentemente do tecido (4), levando a uma melhora na aparência da cicatriz e queloide (5) em até 8 semanas.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="clinicalReference">        
        <div class="reference-content">            
            <div class="container">
                <div class="row">                    
                    <div class="col-12">
                        <div class="reference-desc">
                            <h4>
                                Referência:
                            </h4>
                            <p>
                                1. GALLANT-BEHM L. C., PhD e Mustoe A. T., MD – Occlusion Regulates Epidermal Cytokine Production and Inhibits Scar Formation – NIH, Wound Repair Regen. 2010 ; 18(2): 235–244.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


</main>    