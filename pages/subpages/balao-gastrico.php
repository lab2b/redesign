<main data-controller="balaoGastrico">

    <div class="banner-container">                                                   
        <div class="banner-top">
            <div class="bkgd-overlay"></div>
                <img src="./assets/imagem/banner/subpages/balao-gastrico/banner.jpg" alt="" srcset="">                            
            <div class="text-banner">
                <h2 data-aos="fade-down">Balão Gástrico</h2>
            </div>
            <div class="banner-animation-scrolldown">
                <div class="banner-line"></div>
            </div>
        </div>        
    </div>


    <section id="balaoGastricoProd" class="grayBg">
        <div class="wrapper">
            <div class="row grid-reverse">
                <div class="col-md-7 col-sm-12 grid-positionA px-0">
                    <div class="default-simple-text flex-display" data-aos="fade-up">
                        <p>
                            O Balão Gástrico Silimed é uma opção não cirúrgica no tratamento da obesidade, segura, de uso temporário (até 6 meses) e eficaz para promover o emagrecimento gradual e saudável.
                        </p>
                        <p>
                            Ele é um dispositivo que deve ser acompanhado de mudanças comportamentais e no hábito alimentar do paciente e da prática de exercício físico para auxiliar na perda de peso efetiva. A reeducação dos hábitos diários contribui para a perda de peso durante o tratamento e a mantém por um período prolongado, após a retirada do dispositivo.
                        </p>
                        <p>
                            Para mais informações, acesse nosso site exclusivo.
                        </p>
                        <div class="btn-silimed btn-silimed__center orangeBg">
                                <a href="/">
                                    Balão Gástrico SILIMED
                                </a>
                        </div>
                    </div>
                </div>

                <div class="col-md-5 col-sm-12 grid-positionA px-0 whiteBg">
                    <div class="balao-prod-imagem" >
                        <img src="./assets/imagem/banner/subpages/balao-gastrico/balao-gastrico.jpg" alt="" srcset="">     
                    </div>
                </div>
            </div>
        </div>
    </section>


</main>    