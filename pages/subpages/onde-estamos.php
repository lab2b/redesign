<main data-controller="ondeEstamos">

    <div class="banner-container">                                                   
        <div class="banner-top">
            <div class="bkgd-overlay"></div>
            <img src="./assets/imagem/banner/subpages/onde-estamos/banner.jpg" alt="" srcset="">                
            <div class="text-banner">
                <h2 class="toDown">Onde Estamos</h2>
            </div>
        </div>        
    </div>

    <section id="ondeEstamosTitle">
        <div class="wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="default-desc">
                        <p>
                            A Silimed está presente em diversos países.
                        </p>
                    </div>
                </div>
            </div>        
        </div>    
    </section>



    <section id="mapsA">
        <div class="container">
            <div class="row">
                <div class="col-12">

                </div>
            </div>
        </div>
    </section>

    <section id="mapsB">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <script>
                            function initMap() {
                                // The location of Uluru
                                // const uluru = { lat: -25.344, lng: 131.036 };
                                // The map, centered at Uluru
                                const map = new google.maps.Map(document.getElementById("map"), {
                                zoom: 2.4,
                                center: { lat: 20, lng: 20 },
                                // center: uluru,
                                });
                                // The marker, positioned at Uluru
                                // const marker = new google.maps.Marker({
                                //     position: uluru,
                                //     map: map,
                                // });

                            var iconBase = './assets/imagem/icons/';
                            var icons = {                                
                                silimed: {
                                    icon: iconBase + 'silimed.png'
                                }
                            };

                                function addMarker(feature) {
                                    var marker = new google.maps.Marker({
                                        position: feature.position,
                                        icon: icons[feature.type].icon,
                                        map: map
                                    });
                                }

                                const features = [
                                    {
                                        position: new google.maps.LatLng(21.17197, -86.84764),
                                        type: "silimed",
                                    },
                                    {
                                        position: new google.maps.LatLng(-23.60273, -46.64867),
                                        type: "silimed",
                                    },
                                    {
                                        position: new google.maps.LatLng(-22.80630, -43.31226),
                                        type: "silimed",
                                    },
                                    {
                                        position: new google.maps.LatLng(55.76303, 37.55484),
                                        type: "silimed",
                                    },
                                ];
                                for (let i = 0; i < features.length; i++) {
                                    const marker = new google.maps.Marker({
                                    position: features[i].position,
                                    icon: icons[features[i].type].icon,
                                    map: map,
                                    });
                                }
                            }
                    </script>
                    <div id="map"></div>
                    <!-- Async script executes immediately and must be after any DOM elements used in callback. -->
                    <script
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaTh9bhLSc8TeXAAn2eAFkxreeMNFDpJQ&callback=initMap&libraries=&v=weekly"
                    async
                    >
                </script>
                </div>
            </div>
        </div>
    </section>


</main>    