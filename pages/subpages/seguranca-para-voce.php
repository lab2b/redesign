<main data-controller="segurancaParaVoce">

    <div class="banner-container">                                                   
        <div class="banner-top">
            <div class="bkgd-overlay"></div>
            <img src="./assets/imagem/banner/contato/banner.jpg" alt="" srcset="">                            
            <div class="text-banner">
                <h2 class="toDown">Registro de Paciente</h2>
            </div>
        </div>        
    </div>

    <section id="formRegistro">
        <div class="container">
            <div class="form-card">
                <div class="row">                    
                    <div class="col-md-12 col-sm-12">
                        <div class="form-content">   
                            <form class="row g-3" id="formRegistro">
                                <div class="col-12">
                                    <h2>
                                        Dados do Paciente
                                    </h2>                            
                                </div>
                                <div class="col-md-12">
                                    <label for="inputName" class="form-label"></label>
                                    <input type="text" class="form-control" id="inputName" placeholder="Nome completo">
                                </div>
                                <div class="col-md-6">
                                    <label for="inputAddress" class="form-label"></label>
                                    <input type="text" class="form-control" id="inputAddressName" placeholder="Endereço">
                                </div>
                                <div class="col-md-6">
                                    <label for="inputCity" class="form-label"></label>
                                    <input type="text" class="form-control" id="inputCity" placeholder="Cidade">
                                </div>
                                <div class="col-md-6">
                                    <label for="inputState" class="form-label"></label>
                                    <input type="text" class="form-control" id="inputState" placeholder="Estado">
                                </div>
                                <div class="col-md-6">
                                    <label for="inputRoleCountry" class="form-label">País</label>
                                    <select id="inputRoleCountry" class="form-select">
                                        <option selected value="null">Selecione uma opção</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="inputCep" class="form-label"></label>
                                    <input type="text" class="form-control" id="inputCep" placeholder="CEP">
                                </div>
                                <div class="col-md-6">
                                    <label for="inputEmail" class="form-label"></label>
                                    <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                                </div>
                                <div class="col-md-6">
                                    <label for="inputBrith" class="form-label"> Data de nacimento</label>
                                    <input type="date" class="form-control" id="inputBrith" min="1921-04-01" max="2000-12-31" placeholder="Data de Nacimento">
                                </div>
                                <div class="col-md-6">
                                    <label for="inputRoleEtnia" class="form-label">Etnia</label>
                                    <select id="inputRoleEtnia" class="form-select">
                                        <option selected value="null">Selecione uma opção</option>
                                        <option> Branca</option>
                                        <option> Negra</option>
                                        <option> Asiática</option>
                                        <option> Mestiça</option>
                                    </select>
                                </div>
                                <div class="col-12">
                                    <h2>
                                        Dados do Implante
                                    </h2> 
                                </div>
                                <div class="col-md-6">
                                    <label for="inputRef" class="form-label"></label>
                                    <input type="text" class="form-control" id="inputRef" placeholder="Referência">
                                </div>
                                <div class="col-md-6">
                                    <label for="inputSuperficie" class="form-label"></label>
                                    <input type="text" class="form-control" id="inputSuperficie" placeholder="Superfície">
                                </div>
                                <div class="col-md-6">
                                    <label for="inputRoleSNLeft" class="form-label">SN Esquerdo</label>
                                    <select id="inputRoleSNLeft" class="form-select">
                                        <option selected value="null">Selecione uma opção</option>
                                        <option>Sim</option>
                                        <option>Não se aplica</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="inputRoleSNRight" class="form-label">SN Direito</label>
                                    <select id="inputRoleSNRight" class="form-select">
                                        <option selected value="null">Selecione uma opção</option>
                                        <option>Sim</option>
                                        <option>Não se aplica</option>
                                    </select>
                                </div>


                                <div class="col-12">
                                    <h2>
                                        Dados da Cirurgia
                                    </h2> 
                                </div>
                                <div class="col-md-6">
                                    <label for="inputSurgery" class="form-label"> Data da Cirurgia</label>
                                    <input type="date" class="form-control" id="inpuSurgery" min="1921-04-01" max="2000-12-31" placeholder="Data de Nacimento">
                                </div>
                                <div class="col-md-6">
                                    <label for="inputSurgeryLocal" class="form-label"></label>
                                    <input type="text" class="form-control" id="inputSurgeryLocal" placeholder="Local da cirurgia (nome do hospital/clínica)">
                                </div>
                                <div class="col-md-6">
                                    <label for="inputRoleSurgeryCountry" class="form-label">País</label>
                                    <select id="inputRoleSurgeryCountry" class="form-select">
                                        <option selected value="null">Selecione uma opção</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="inputSurgeryState" class="form-label"></label>
                                    <input type="text" class="form-control" id="inputSurgeryState" placeholder="Estado">
                                </div>
                                <div class="col-md-6">
                                    <label for="inputSurgeryCity" class="form-label"></label>
                                    <input type="text" class="form-control" id="inputSurgeryCity" placeholder="Cidade">
                                </div>
                                <div class="col-md-6">
                                    <label for="inputRolePosition" class="form-label">Posição do implante</label>
                                    <select id="inputRolePosition" class="form-select">
                                        <option selected value="null">Selecione uma opção</option>
                                        <option>Subglandular</option>
                                        <option>Retromuscular (atrás do musculo)</option>
                                        <option>Intramuscular</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="inputDoctor" class="form-label"></label>
                                    <input type="text" class="form-control" id="inputDoctor" placeholder="Nome do médico">
                                </div>
                                <div class="col-md-6">
                                    <label for="inputRoleReason" class="form-label">Indicação cirúrgica</label>
                                    <select id="inputRoleReason" class="form-select">
                                        <option selected value="null">Selecione uma opção</option>
                                        <option>Aumento</option>
                                        <option>Reconstrução </option>
                                        <option>Troca de implante</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="inputRoleIncision" class="form-label">Incisão</label>
                                    <select id="inputRoleIncision" class="form-select">
                                        <option selected value="null">Selecione uma opção</option>
                                        <option>Aerolar</option>
                                        <option>Inframamária </option>
                                        <option>Axilar</option>
                                    </select>
                                </div>
                                <div class="col-12">
                                    <div class="submit-content">
                                        <small id="emailHelp" class="form-text text-muted">*Seus dados não serão compartilhados.</small>
                                        <button type="submit" class="btn-silimed orangeBg btn-submit">Enviar Cadastro</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>