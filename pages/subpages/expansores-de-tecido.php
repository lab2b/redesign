<main data-controller="expansoresDeTecido">

    <div class="banner-container">                                                   
        <div class="banner-top">
            <div class="bkgd-overlay"></div>
                <img src="./assets/imagem/banner/subpages/expansores-de-tecido/banner.jpeg" alt="" srcset="">                            
            <div class="text-banner">
                <h2 data-aos="fade-down">Expansores de Tecido</h2>
            </div>
            <div class="banner-animation-scrolldown">
                <div class="banner-line"></div>
            </div>
        </div>        
    </div>


    <section id="expansoresProd">
        <div class="wrapper">
            <div class="row grid-reverse">
                <div class="col-md-7 col-sm-12 grid-positionA px-0 orangeBg">
                    <div class="expansor-prod-text flex-display" data-aos="fade-up">
                        <p>
                            Os expansores de tecido Silimed são constituídos por membrana de elastômero de silicone. Suas indicações são múltiplas, desde reconstruções mamárias pós-mastectomia até correção das sequelas de queimaduras, passando por cirurgia de calvície, tumores faciais e revisão de cicatrizes em geral. Eles são disponibilizados com 3 válvulas, redonda adulta e infantil e luer lock. A expansão do tecido mamário, realizado após a mastectomia, oferece vantagens significativas:
                        </p>
                        <ul>
                            <li>
                                Não afeta ou altera a cor, a textura, a vascularização e a inervação do tecido que foi expandido;
                            </li>
                            <li>
                                Permite um melhor planejamento e acompanhamento da restauração;
                            </li>
                            <li>
                                Assegura um resultado mais natural, seguro e confortável para a mulher.
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-5 col-sm-12 grid-positionB whiteBg px-0">
                    <div class="expansor-prod-imagem flex-display" data-aos="zoom-in-left">
                        <img src="./assets/imagem/banner/subpages/expansores-de-tecido/expansor.jpg" alt="" srcset="">                            
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="expansoresValvulas" class="grayBg">
        <div class="wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="valvula-title">
                        <h2>
                            Tipos de válvulas remotas
                        </h2>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="valvula-area flex-display">
                        <div class="valvula-img" data-aos="fade-up">
                            <img src="./assets/imagem/banner/subpages/expansores-de-tecido/example-a.png" alt="" srcset=""> 
                        </div>
                        <div class="valvula-desc">
                            <h4>
                                VÁLVULA REDONDA
                            </h4>
                            <div class="paragraph-area" data-aos="fade-up">
                                <p>
                                    Possui um disco de aço inox interno para evitar o transpasse da agulha e
                                </p>
                                <p>
                                    facilitar a localização por radiografia.
                                </p>
                                <p>
                                    Diâmetro da base: 34mm (adulto); 22mm (infantil) Altura: 11mm (adulto); 8mm (infantil)
                                </p>                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-12">
                    <div class="valvula-area flex-display">
                        <div class="valvula-img" data-aos="fade-up">
                            <img src="./assets/imagem/banner/subpages/expansores-de-tecido/example-b.png" alt="" srcset=""> 
                        </div>
                        <div class="valvula-desc">
                            <h4>
                                LUER LOCK
                            </h4>
                            <div class="paragraph-area" data-aos="fade-up">
                                <p>
                                    Válvula com encaixe roscado para conexão direta na seringa sem agulha. 
                                </p>
                                <p>
                                    Possui um sistema de vedação que impede a saída de líquido 
                                </p>
                                <p>
                                    automaticamente ao desconectar a seringa.
                                </p>
                            </div>
                        </div>                                
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>    