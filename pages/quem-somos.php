
<main data-controller="quemSomos">

<div class="banner-container">                                                   
    <div class="banner-top">
        <div class="bkgd-overlay"></div>
            <img src="./assets/imagem/banner/quem-somos/banner.jpg" alt="" srcset="">                            
        <div class="text-banner">
            <h2 data-aos="fade-down">Quem Somos</h2>
        </div>
        <div class="banner-animation-scrolldown">
            <div class="banner-line"></div>
        </div>
    </div>        
</div>

    <section id="quem-somos">        
        <div class="quem-somos-content">            
            <div class="container">
                <div class="row">                    
                    <div class="col-12">
                        <div class="default-desc" data-aos="fade-up">
                            <p>
                                PRESENTE NO MERCADO DESDE 1978,
                            </p>
                            <p>
                                A SILIMED é <span> a maior fabricante de implantes </span> 
                            </p>
                            <p>
                                <span>
                                    de silicone da América Latina e líder em vendas no mercado brasileiro.
                                </span>
                            </p>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="aboutUs">
        <div class="wrapper">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="content-about-us" data-aos="fade-up">
                        <div class="default-simple-text default-simple-text--gray">                        
                            <h2>
                                Sobre Nós
                            </h2>
                            <p>
                                A Silimed tem por vocação contribuir com a cirurgia plástica, promovendo tanto pesquisas quanto estudos cientíﬁcos, e desenvolvendo tecnologias para atender diferentes necessidades. A Silimed foi a primeira empresa no mundo a identiﬁcar cada implante com um número de série individual, permitindo a rastreabilidade dos produtos e proporcionando mais segurança a médicos e pacientes.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <!-- <div class="img-about" data-aos="zoom-in-left"> -->
                    <div class="img-about img-srolling">
                        <img src="./assets/imagem/banner/quem-somos/numero-de-serie.jpg" alt="" srcset=""> 
                    </div>
                </div>
            </div>
        </div>      
    </section>



    <section id="mission">
        <div class="wrapper">
            <div class="row grid-reverse">
                <div class="col-md-5 col-sm-12 grid-positionA">
                    <div class="img-mission"> <!--  data-aos="zoom-in-right" -->
                        <img src="./assets/imagem/banner/quem-somos/banner.jpg" alt="" srcset="">             
                    </div>
                </div>
                <div class="col-md-7 col-sm-12 grid-postitionB">
                    <div class="content-mission" data-aos="fade-up">
                        <div class="default-simple-text">
                            <h2>
                                Missão
                            </h2>
                            <p>
                                Ser para a comunidade médica a principal referência mundial na fabricação de produtos médicos de silicone, através de uma rede de distribuidores capacitados, promovendo o bem-estar de pacientes e de todos os stakeholders (interessados).
                            </p>                          
                        </div>                        
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
    <section id="aboutFactory">
        <div class="about-factory">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="content-factory" data-aos="fade-up">
                            <div class="default-simple-text">
                                <h2>
                                    Nova Fábrica​
                                </h2>
                                <p>
                                    Localizada em Duque de Caxias (RJ), a nova planta com área de 12.000 m2, conta com uma estrutura moderna, incluindo uma sala limpa com equipamentos de última geração, o que possibilita um processo contínuo desde o recebimento da matéria-prima até a saída do produto ﬁnal. Desta maneira, atenderemos aos mais altos padrões mundiais de qualidade e segurança com os produtos Silimed. A inauguração da nova fábrica proporcionará o retorno a mercados internacionais e a ampliação da venda na América Latina.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ul>
                <li>
                    <div class="img-factory">
                        <img src="./assets/imagem/banner/quem-somos/factory-1.jpg" alt="" srcset="">
                    </div>
                </li>
                <li>
                    <div class="img-factory">
                        <img src="./assets/imagem/banner/quem-somos/factory-2.jpg" alt="" srcset="">
                    </div>
                </li>
                <li>
                    <div class="img-factory">
                        <img src="./assets/imagem/banner/quem-somos/factory-3.jpg" alt="" srcset="">
                    </div>         
                </li>
            </ul>
        </div>
    </section>



</main>