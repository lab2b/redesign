
<main data-controller="menuCopy">

    <div class="banner-container">                                                   
        <div class="banner-top">
            <div class="bkgd-overlay"></div>
            <img src="./assets/imagem/banner/subpages/implantes-mamarios/banner.jpg" alt="" srcset="">                            
            <div class="text-banner">
                <h2 data-aos="fade-down">Linha Standard</h2>
            </div>
            <div class="banner-animation-scrolldown">
                <div class="banner-line"></div>
            </div>
        </div>        
    </div>
    <section class="menucopy">

        <div class="page-wrapper chiller-theme toggled">
            <a id="show-sidebar" class="menu-responsive-hmbg   btn btn-sm btn-dark" href="#">
                <i class="fas fa-bars"></i>
            </a>
            <nav id="sidebar" class="sidebar-wrapper ">  <!-- container  -->
                <div class="sidebar-content">
                    <div class="sidebar-brand">
                        <a href="/home">
                            <picture>
                                <source srcset="./assets/imagem/logos-silimed-colorido.png" media="(max-width: 767px)">
                                <source srcset="./assets/imagem/logos-silimed.png">
                                <img srcset="./assets/imagem/logos-silimed.png" alt="Logo Silimed">
                            </picture>
                        </a>
                        <div id="close-sidebar">
                        <i class="fas fa-times"></i>
                    </div>
                </div>

                <div class="sidebar-menu">
                    <ul>
                        <li>
                            <div class="sillimed-menu-logo">
                                <a href="/home">
                                    <picture>
                                        <source srcset="./assets/imagem/logos-silimed-colorido.png" media="(max-width: 767px)">
                                        <source srcset="./assets/imagem/logos-silimed.png">
                                        <img srcset="./assets/imagem/logos-silimed.png" alt="Logo Silimed">
                                    </picture>
                                </a>
                            </div>
                        </li>
                        <li class="sidebar-dropdown sidebar-dropdown__haschild">
                            <a href="/quem-somos"> Quem Somos </a>
                            <span></span>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="/certificacoes">
                                            Certificações
                                        </a>    
                                    </li>
                                    <li>
                                        <a href="/nossa-historia">
                                            Nossa História
                                        </a>
                                    </li>
                                    <li>
                                        <a href="onde-estamos">
                                            Onde Estamos
                                        </a>
                                    </li>              
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown sidebar-dropdown__haschild">
                            <a href="/produtos">Produtos</a>
                            <span></span>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="/implantes-mamarios">
                                            Implates Mamários
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/implantes-de-contorno-corporal">
                                            Implantes de Contorno Corporal
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/implantes-faciais-de-mento">
                                            Implantes Faciais (de mento)
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/implantes-da-linha-de-urologia">
                                            Implantes de Linha de Urologia
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/modelador-nasal">
                                            Modelador Nasal
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/expansores-de-tecido">
                                            Expansores de Tecido
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/medgel">
                                            Medgel                                    
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/balao-gastrico">
                                            Balão Gástrico
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown sidebar-dropdown__haschild">
                            <a href="pedidos-de-compra"> Pedidos de Compra</a>
                            <span></span>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="/para-medicos">Para Médicos</a>
                                    </li>
                                    <li>
                                        <a href="/para-pacientes">Para Pacientes</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="/estudo-clinico"> Estudo Clínico </a>
                            <span></span>
                        </li>
                        <li class="sidebar-dropdown sidebar-dropdown__haschild">
                            <a href="/pacientes">Pacientes</a>
                            <span></span>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="/seguranca-para-voce">
                                            Cadastre-se
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/pacientes#psps">
                                            PSPS
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="/carreiras"> Carreiras </a>
                            <span></span>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="/contato"> Contato </a>
                            <span></span>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

    </section>

</main>
