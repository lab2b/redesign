<main data-controller="home">
    <section id="home-page">
        <!-- banner -->
        <div class="banner-carrousel ">
            <div class="swiper-container mySwiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">                        
                        <img src="./assets/imagem/banner/banner-mamarios.jpg" alt="" srcset="">                            
                        <div class="text-banner">
                        <h2 class="  ">Ciência & Bem-Estar</h2>
                            <p> Produtos para diferentes biotipos</p>
                            <div class="btn-banner   ">
                                <a href="http://">
                                    Conheça
                                </a>                        
                            </div>
                        </div>                        
                    </div>
                    <div class="swiper-slide">
                        <img src="./assets/imagem/banner/banner-mamarios-3.jpg" alt="" srcset="">                            
                        <div class="text-banner">
                            <h2 class="  ">Silimed Acamedemy</h2>
                            <p> Programa para Atualização Científica</p>
                            <div class="btn-banner   ">
                                <a href="http://">
                                    Conheça
                                </a>                        
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <img src="./assets/imagem/banner/banner-mamarios-2.jpg" alt="" srcset="">
                        <div class="text-banner">
                            <h2 class="  ">Pedido de Compra Online</h2>
                            <p>Atendimento diferenciado e automatizado</p>
                            <div class="btn-banner   ">
                                <a href="http://">
                                    Conheça
                                </a>                        
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container relativeP">
                    <div class="carousel-controller">
                        <div class="swiper-pagination"></div>
                        <div class="arrow-swiper-container">
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
            <!-- Inicio -->
        <div class="produto-carousel">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="carousel-produtos-home">
                            <div class="default-desc">
                                <h1>
                                    Nossos Produtos
                                </h1>
                                <p>
                                    Uma linha completa de implantes de silicone para diferentes indicações, que proporciona uma melhor relação com sua autoestima e confiança.
                                </p>
                            </div>
                            <div class="swiper-container homeSwiper" data-aos="fade-up">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="item-carousel">
                                            <img src="./assets/imagem/home/item-silimed.jpeg" alt="" srcset="">
                                            <div class="title-prod-carousel">
                                                <h3>
                                                    implantes mámarios
                                                </h3>
                                            </div>
                                        </div>                                    
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="item-carousel">
                                            <img src="./assets/imagem/home/item-silimed-3.jpg" alt="" srcset="">
                                            <div class="title-prod-carousel">
                                                <h3>
                                                    implantes mámarios
                                                </h3>
                                            </div>
                                        </div>                                    
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="item-carousel">
                                            <img src="./assets/imagem/home/item-silimed-2.jpeg" alt="" srcset="">
                                            <div class="title-prod-carousel">
                                                <h3>
                                                    implantes mámarios
                                                </h3>
                                            </div>
                                        </div>                                    
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="item-carousel">
                                            <img src="./assets/imagem/home/item-silimed-3.jpg" alt="" srcset="">
                                            <div class="title-prod-carousel">
                                                <h3>
                                                    implantes mámarios
                                                </h3>
                                            </div>
                                        </div>                                    
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="item-carousel">
                                            <img src="./assets/imagem/home/item-silimed.jpeg" alt="" srcset="">
                                            <div class="title-prod-carousel">
                                                <h3>
                                                    implantes mámarios
                                                </h3>
                                            </div>
                                        </div>                                    
                                    </div>

                                    <div class="swiper-slide">
                                        <div class="item-carousel">
                                            <img src="./assets/imagem/home/item-silimed.jpeg" alt="" srcset="">
                                            <div class="title-prod-carousel">
                                                <h3>
                                                    implantes mámarios
                                                </h3>
                                            </div>
                                        </div>                                    
                                    </div>

                                    <div class="swiper-slide">
                                        <div class="item-carousel">
                                            <img src="./assets/imagem/home/item-silimed.jpeg" alt="" srcset="">
                                            <div class="title-prod-carousel">
                                                <h3>
                                                    implantes mámarios
                                                </h3>
                                            </div>
                                        </div>                                    
                                    </div>

                                    <div class="swiper-slide">
                                        <div class="item-carousel">
                                            <img src="./assets/imagem/home/item-silimed.jpeg" alt="" srcset="">
                                            <div class="title-prod-carousel">
                                                <h3>
                                                    implantes mámarios
                                                </h3>
                                            </div>
                                        </div>                                    
                                    </div>
                                    
                                </div>
                                <br>
                                <div class="swiper-pagination"></div>
                            </div>


                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        <div class="side-instagram-carousel">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>
                            <a href="https://instagram.com">
                                @silimed.official
                            </a>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="carousel-instagram">
                
                <div class="swiper-container instaSwiper">
                    
                    <div class="swiper-wrapper">


                        <div class="swiper-slide">
                            <div class="item-insta">
                                <img src="./assets/imagem/insta/insta-item.jpg" alt="" srcset="">
                                <div class="insta-mask">
                                    <div class="insta-desc">
                                        <div class="social-icon">
                                            <i class="insta-item insta-item__icon  far fa-heart "></i>
                                            <p class="insta-item insta-item__number">
                                                50
                                            </p>
                                            <i class="insta-item insta-item__icon far fa-comment-alt "></i>
                                            <p class="insta-item insta-item__number">
                                                50
                                            </p>
                                        </div>
                                        <div class="social-text">
                                            <p>
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut eveniet perferendis voluptatem doloremque adipisci, sed magni repudiandae sit exercitationem voluptas eaque aspernatur accusantium tempora quod suscipit saepe iste. Doloremque, similique!
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide">
                            <div class="item-insta">
                                <img src="./assets/imagem/insta/insta-item.jpg" alt="" srcset="">
                                <div class="insta-mask">
                                    <div class="insta-desc">
                                        <div class="social-icon">
                                            <i class="insta-item insta-item__icon  far fa-heart "></i>
                                            <p class="insta-item insta-item__number">
                                                50
                                            </p>
                
                                        </div>
                                        <div class="social-text">
                                            <p>
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut eveniet perferendis voluptatem doloremque adipisci, sed magni repudiandae sit exercitationem voluptas eaque aspernatur accusantium tempora quod suscipit saepe iste. Doloremque, similique!
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide">
                            <div class="item-insta">
                                <img src="./assets/imagem/insta/insta-item.jpg" alt="" srcset="">
                                <div class="insta-mask">
                                    <div class="insta-desc">
                                        <div class="social-icon">
                                            <i class="insta-item insta-item__icon  far fa-heart "></i>
                                            <p class="insta-item insta-item__number">
                                                50
                                            </p>
                                            <i class="insta-item insta-item__icon far fa-comment-alt "></i>
                                            <p class="insta-item insta-item__number">
                                                50
                                            </p>
                                        </div>
                                        <div class="social-text">
                                            <p>
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut eveniet perferendis voluptatem doloremque adipisci, sed magni repudiandae sit exercitationem voluptas eaque aspernatur accusantium tempora quod suscipit saepe iste. Doloremque, similique!
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="swiper-slide">
                            <div class="item-insta">
                                <img src="./assets/imagem/insta/insta-item.jpg" alt="" srcset="">
                                <div class="insta-mask">
                                    <div class="insta-desc">
                                        <div class="social-icon">
                                            <i class="insta-item insta-item__icon  far fa-heart "></i>
                                            <p class="insta-item insta-item__number">
                                                50
                                            </p>
                                        </div>
                                        <div class="social-text">
                                            <p>
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut eveniet perferendis voluptatem doloremque adipisci, sed magni repudiandae sit exercitationem voluptas eaque aspernatur accusantium tempora quod suscipit saepe iste. Doloremque, similique!
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide">
                            <div class="item-insta">
                                <img src="./assets/imagem/insta/insta-item.jpg" alt="" srcset="">
                                <div class="insta-mask">
                                    <div class="insta-desc">
                                        <div class="social-icon">
                                            <i class="insta-item insta-item__icon  far fa-heart "></i>
                                            <p class="insta-item insta-item__number">
                                                50
                                            </p>
                                        </div>
                                        <div class="social-text">
                                            <p>
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut eveniet perferendis voluptatem doloremque adipisci, sed magni repudiandae sit exercitationem voluptas eaque aspernatur accusantium tempora quod suscipit saepe iste. Doloremque, similique!
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        


                    </div>
                    <div class="carousel-controller">
                        <div class="arrow-swiper-container">
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>                        
                    </div>
                </div>



            </div>
        </div>
    
    
    </section>
</main>